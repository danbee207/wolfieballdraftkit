/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf;

/**
 * This class stores all the constants used by the Course Site Builder application 
 * at startup, which means before the user interface is even loaded. This mostly 
 * means how to find files for initializing the application, like properties.xml.
 * 
 * @author DnB
 */
public class WDK_StartupConstants {
    
    //WE NEED THESE CONSTANTS JUST TO GET STARTED
    //LOADING SETTINGS FROM OUR XML FILES
    
    public static final String PROPERTIES_FILE_NAME = "properties.xml";
    public static final String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";    
    public static final String PATH_DATA = "./data/";
    public static final String PATH_DRAFT = PATH_DATA + "drafts/";
    public static final String PATH_PLAYER = PATH_DATA+"players/";
    public static final String PATH_IMAGES = PATH_DATA + "img/";
    public static final String PATH_CSS = "wdf/css/";
    public static final String PATH_SITES = "sites/";
    public static final String PATH_BASE = PATH_SITES + "base/";
    public static final String PATH_EMPTY = ".";
    public static final String PATH_PLAYER_IMAGES = PATH_IMAGES + "players/";
    public static final String PATH_FLAG_IMAGES = PATH_IMAGES + "flags/";

    // THESE ARE THE DATA FILES WE WILL LOAD AT STARTUP
    public static final String JSON_FILE_PATH_HITTERS = PATH_PLAYER + "Hitters.json";
    public static final String JSON_FILE_PATH_PITCHERS = PATH_PLAYER + "Pitchers.json";
    
    // ERRO MESSAGE ASSOCIATED WITH PROPERTIES FILE LOADING ERRORS
    public static String PROPERTIES_FILE_ERROR_MESSAGE = "Error Loading properties.xml";

    // ERROR DIALOG CONTROL
    public static String CLOSE_BUTTON_LABEL = "Close";
}
