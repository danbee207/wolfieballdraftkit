package wdf;

import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wdf.WDK_PropertyType.PROP_APP_TITLE;
import static wdf.WDK_StartupConstants.*;
import wdf.controller.PlayerEditController;
import wdf.data.Player;
import wdf.error.FileErrorHandler;
import wdf.error.InitErrorHandler;
import wdf.file.DraftSiteExporter;
import wdf.file.JsonWolfieFileManager;
import wdf.gui.WDK_GUI;
import wdf.manager.DraftDataManager;
import wdf.manager.PlayerDataManager;
import xml_utilities.InvalidXMLFileFormatException;

/**
 * WolfieballDraftKits is a JavaFX application that can be used to build the
 * draft for a baseball teams. 
 * @author DnB
 */
public class WolfieballDraftKits extends Application {
  
    // THIS IS THE FULL USER INTERFACE, WHICH WILL BE INITIALIZED
    // AFTER THE PROPERTIES FILE IS LOADED 
   WDK_GUI gui; 
   
   
    /**
     * This is where our Application begins its initialization, it will
     * create the GUI and initialize all of its components.
     * 
     * @param primaryStage This application's window.
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
       
        FileErrorHandler eH = FileErrorHandler.getErrorHandler();
        InitErrorHandler ieH = InitErrorHandler.getErrorHandler();
        ieH.initMessageDialog(primaryStage);
        
        //LOAD APP SETTINGS INTO THE GUI AND START IT UP
        boolean success = loadProperties();
        if(success){
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
            
            try{
                //WE WILL SAVE OUR COURSE DATA USING THE JSON FILE
                //FORMAT SO WE'LL LET THIS OBJECT DO THIS FOR US
                JsonWolfieFileManager jsonFileManager = new JsonWolfieFileManager();
            
                //ANT THIS ONE WILL DO THE COURSE WEB PAGE EXPORTING
                DraftSiteExporter expoter = new DraftSiteExporter(PATH_BASE, PATH_SITES);
                
                ObservableList<Player> hitters = jsonFileManager.loadPlayers(JSON_FILE_PATH_HITTERS,"Hitters");
                ObservableList<Player> pitchers = jsonFileManager.loadPlayers(JSON_FILE_PATH_PITCHERS,"Pitchers");
                
                //AND NOW GIVE ALL OF THIS STUFF TO THE GUI
                //INITIALIZE THE USER INTERFACE COMPONENTS
                gui = new WDK_GUI(primaryStage);
                gui.setWolfieballFileManager(jsonFileManager);
                gui.setSiteExporter(expoter);
                        
                // CONSTRUCT THE DATA MANAGER AND GIVE IT TO THE GUI
                
                PlayerDataManager playerManager = new PlayerDataManager(pitchers, hitters);
                DraftDataManager draftManager = new DraftDataManager(gui,hitters,pitchers,playerManager.getAllplayers());
                
                gui.setDataManager(draftManager);
                gui.setPlayermanager(playerManager);
                
                // FINALLY, START UP THE USER INTERFACE WINDOW AFTER ALL
                // REMAINING INITIALIZATION
                gui.initGUI(appTitle,hitters,pitchers);
                
            }catch(IOException ioe){
                eH = FileErrorHandler.getErrorHandler();
                eH.handlePropertiesFileError();
            }
        }
    }

    private boolean loadProperties() {
        try{
            //LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);

            return true;
            
        }catch(InvalidXMLFileFormatException ixmlffe){
            FileErrorHandler eH = FileErrorHandler.getErrorHandler();
            eH.handlePropertiesFileError();
            return false;
        }
    
    }
    
    
     /**
     * This is where program execution begins. Since this is a JavaFX app
     * it will simply call launch, which gets JavaFX rolling, resulting in
     * sending the properly initialized Stage (i.e. window) to our start
     * method in this class.
     */
    public static void main(String[] args) {
        launch(args);
    }


    
}
