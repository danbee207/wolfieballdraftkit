/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.stage.Stage;
import wdf.data.EstimatedValueComparator;
import wdf.data.Team;
import wdf.data.TeamMember;
import wdf.gui.DraftScreen;
import wdf.gui.MessageDialog;
import wdf.gui.ProgressDialog;
import wdf.gui.WDK_GUI;
import wdf.gui.YesNoCancelDialog;

/**
 *
 * @author DnB
 */
public class DraftEditController {

    // WE USE THIS TO MAKE SURE OUR PROGRAMMED UPDATES OF UI
    // VALUES DON'T THEMSELVES TRIGGER EVENTS
    private boolean enabled;
    Stage primaryStage;
    WDK_GUI wdk_gui;
    MessageDialog msd;
    YesNoCancelDialog yncd;
    ProgressDialog pgd;
    Thread thread;
    boolean isPlayer;
    String autoPos;
    
    public volatile boolean stopRequested = false;

    boolean isLeftTeam = true;
    Team teamToSelect;
    TeamMember tMemberToSelected;

    /**
     * Constructor that gets this controller ready, not much to initialize as
     * the methods for this function are sent all the objects they need as
     * arguments.
     */
    public DraftEditController(Stage initPrimaryStage, MessageDialog initMessageDialog, YesNoCancelDialog inityesnoCancelDialog, WDK_GUI initgui) {
        enabled = true;

        primaryStage = initPrimaryStage;
        msd = initMessageDialog;
        yncd = inityesnoCancelDialog;

        wdk_gui = initgui;

        pgd = new ProgressDialog(primaryStage, "Automating draft");

    }

    public void enable(boolean enableSetting) {
        enabled = enableSetting;
    }

    public void handleAutomatingPlayers(DraftScreen gui) {

        Task<Void> task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                ObservableList<TeamMember> freeagent = gui.getDdataManager().getDraft().freeagentsList();
                ObservableList<Team> teams = gui.getDdataManager().getDraft().getTeams();
                stopRequested = true;

                Thread.sleep(1000);
                while (stopRequested) {

                    if (teams.size() == 0) {
                        msd.show("There is no TEam in this draft");
                        stopRequested = false;
                        break;
                    } else {
                        stopRequested = true;
                    }

                    autoPos = null;
                    teamToSelect = null;
                    tMemberToSelected = null;
                    isPlayer=false;
                    for (Team t : teams) {
                        if (t.getNumLeft() > 0) {
                            teamToSelect = t;
                            isPlayer = true;
                            break;
                        }

                    }
                    System.out.println("Is Player" + isPlayer + " teamToSelect " + teamToSelect);
                    if (teamToSelect == null) {
                        isPlayer = false;

                    }

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            if (isPlayer) {

                                boolean ch = false;
                                Random rd = new Random();
                                
                                ArrayList<String> positionInTeam = getLeftPositions(teamToSelect);
                                
                                for (TeamMember t : freeagent) {
                                    String[] tPoss = t.getQp().split("_");
                                    for (String tp : tPoss) {
                                        int num = rd.nextInt(positionInTeam.size());
                                        System.out.println("PositionInTeam data ; "+positionInTeam.get(num));
                                        if (positionInTeam.get(num).equals(tp)) {
                                            ch = true;
                                            tMemberToSelected = t;
                                            autoPos = positionInTeam.get(num);
                                            
                                            break;
                                        }
                                    }
                                    if (ch) {
                                        break;
                                    }
                                }
                                System.out.println("run autoPos : " + autoPos);
                                tMemberToSelected.setPostion(autoPos);
                                tMemberToSelected.setContract("S2");
                                tMemberToSelected.setSalary(1);

                                teamToSelect.addPlayer(tMemberToSelected);
                                teamToSelect.setSalaryMinus(tMemberToSelected.getSalary());
                                tMemberToSelected.setFantasyTeam(teamToSelect.getName());
                               
                                setnumLeftPlayerPosDow(autoPos, teamToSelect);
                                
                                gui.getDdataManager().getDraft().getFreeagent().removePlayer(tMemberToSelected);
                                gui.getDdataManager().getDraft().addDrafted(tMemberToSelected);

                            } else {
                                
                                for (int i=1;i<teams.size();i++) {
                                    if (teams.get(i).getTaxinum() < 8) {
                                        teamToSelect = teams.get(i);
                                        break;
                                    }
                                }
                                
                                if (teamToSelect != null) {
                                    Random rd = new Random();
                                    tMemberToSelected = freeagent.get(rd.nextInt(freeagent.size()));

                                    tMemberToSelected.setPostion("H");
                                    tMemberToSelected.setContract("X");
                                    tMemberToSelected.setSalary(0);

                                    teamToSelect.addNewTaxi(tMemberToSelected);
                                    
                                    tMemberToSelected.setFantasyTeam(teamToSelect.getName());
                                    
                                    
                                    gui.getDdataManager().getDraft().getFreeagent().removePlayer(tMemberToSelected);
                                    gui.getDdataManager().getDraft().addDrafted(tMemberToSelected);

                                } else {
                                    stopRequested = false;
                                    msd.show("There is no team to automate draft");

                                }

                            }
                            

                        }

                    });
                    
                    Thread.sleep(1000);
                }

                return null;
            }

        };

        thread = new Thread(task);

        thread.start();
    }

    public void handleAutomatingPause(DraftScreen gui) {
        if (stopRequested) {
            stopRequested = false;
            thread.stop();
        } else {
            msd.show("There is no automation!");

        }

    }

    public void handleAutomatingPerson(DraftScreen gui) {

        ObservableList<TeamMember> freeagent = gui.getDdataManager().getDraft().freeagentsList();
        ObservableList<Team> teams = gui.getDdataManager().getDraft().getTeams();
        Team teamToInsert = null;
        isLeftTeam = true;

        if (teams.size() == 0) {
            msd.show("There is no Team in this draft!");
            isLeftTeam = false;
            return;
        }

        //get team which needs to get a player
        for (int i = 1; i < teams.size(); i++) {
            if (teams.get(i).getNumLeft() > 0) {
                teamToInsert = teams.get(i);
                break;
            }
        }

        //get team which needs to get a taxi player
        if (teamToInsert == null) {

            for (int i = 1; i < teams.size(); i++) {
                if (teams.get(i).getTaxiMember().size() < 9) {
                    teamToInsert = teams.get(i);
                    break;
                }
            }

            if (teamToInsert == null) {
                msd.show("There is no team to need to draft!");
                isLeftTeam = false;
                return;
            } else {
                TeamMember itemToSelected = null;
                itemToSelected = findTaxitoInsert(freeagent);
                setOneTaxiDrafted(itemToSelected, teamToInsert, gui);
            }

        } else {
           

            ArrayList<String> pos = getLeftPositions(teamToInsert);

            initFreeagentEValue(freeagent, gui.getDdataManager().getDraft().getRemaingMoney());

            TeamMember itemToSelected = null;
            String realPos = null;
            for (String p : pos) {
                TeamMember newitem;
                newitem = findPlayertoInsert(p, freeagent);
                if (itemToSelected == null || !newitem.getFirst().equals(itemToSelected.getFirst())) {
                    realPos = p;
                    itemToSelected = newitem;
                }
            }

            setOnePlayerDrafted(itemToSelected, teamToInsert, realPos, gui);

        }

    }

    private ArrayList<String> getLeftPositions(Team teamToInsert) {

        ArrayList<String> pos = new ArrayList();

        for (int i = 0; i < 10; i++) {
            if (teamToInsert.getNumPos(i) > 0) {

                pos.add(getPosition(i));
                
                if (pos.get(0).equals("CI")) {
                    
                    
                    pos.clear();
                    pos.add("1B");
                    pos.add("3B");

                } else if (pos.get(0).equals("MI")) {
                    
                 
                    
                    pos.clear();
                    pos.add("2B");
                    pos.add("SS");

                } else if (pos.get(0).equals("U")) {
                   
                    
                    pos.clear();
                    pos.add("C");
                    pos.add("1B");
                    pos.add("3B");
                    pos.add("2B");
                    pos.add("SS");
                    pos.add("OF");

                }
                return pos;
            }
        }
        return null;
    }

    private TeamMember findTaxitoInsert(ObservableList<TeamMember> freeagent) {
        TeamMember selected = null;
        for (TeamMember t : freeagent) {
            if (selected == null || selected.getEstimatedValue() < t.getEstimatedValue()) {
                selected = t;
            }
        }
        return selected;
    }

    private TeamMember findPlayertoInsert(String p, ObservableList<TeamMember> freeagent) {

       
        TeamMember selected = null;
        for (TeamMember t : freeagent) {
            boolean ch = false;
            String[] poss = t.getQp().split("_");
            for (String i : poss) {
                if (i.equals(p)) {
                    ch = true;
                    break;
                }
            }
            if (ch) {
                if (selected == null || selected.getEstimatedValue() < t.getEstimatedValue()) {
                    selected = t;
                }
            }
        }

        return selected;

    }

    private void setOneTaxiDrafted(TeamMember itemToSelected, Team itemToInsert, DraftScreen gui) {

        gui.getDdataManager().getDraft().getFreeagent().removePlayer(itemToSelected);

        itemToSelected.setSalary(0);
        itemToSelected.setContract("X");
        itemToSelected.setPostion("H");
        itemToSelected.setFantasyTeam(itemToInsert.getName());
        itemToInsert.addNewTaxi(itemToSelected);

        gui.getDdataManager().getDraft().addDrafted(itemToSelected);

    }

    private void setOnePlayerDrafted(TeamMember itemToSelected, Team itemToInsert, String pos, DraftScreen gui) {
        gui.getDdataManager().getDraft().getFreeagent().removePlayer(itemToSelected);

        itemToSelected.setSalary(1);
        itemToSelected.setContract("S2");
        itemToSelected.setPostion(pos);
        itemToSelected.setFantasyTeam(itemToInsert.getName());
        setnumLeftPlayerPosDow(pos, itemToInsert);
        itemToInsert.addPlayer(itemToSelected);
        itemToInsert.setSalaryMinus(itemToSelected.getSalary());
        

        gui.getDdataManager().getDraft().addDrafted(itemToSelected);
    }

    private String getPosition(int i) {
        switch (i) {
            case 0:
                return "C";
            case 1:
                return "1B";
            case 2:
                return "CI";
            case 3:
                return "3B";
            case 4:
                return "2B";
            case 5:
                return "MI";
            case 6:
                return "SS";
            case 7:
                return "U";
            case 8:
                return "OF";
            case 9:
                return "P";
        }
        return null;
    }

    private void initFreeagentEValue(ObservableList<TeamMember> freeagent, int remainingMoney) {

        for (TeamMember t : freeagent) {
            t.setEstimatedValue(1.0);
        }
        Collections.sort(freeagent, new EstimatedValueComparator());

        for (TeamMember t : freeagent) {
            t.setEstimatedValue(remainingMoney / t.getEstimatedValue());
        }

    }

    private void setnumLeftPlayerPosDow(String pos, Team team) {

        boolean ch = false;

        if (pos.equals("P") && team.getNumPos(9) > 0) {
            team.setNumPosDown(9);
            ch = true;
        } else if (pos.equals("C") && team.getNumPos(0)>0) {
            team.setNumPosDown(0);
            ch = true;
        } else if (pos.equals("1B") && team.getNumPos(1) > 0) {
            team.setNumPosDown(1);
            ch = true;
        } else if (pos.equals("3B") && team.getNumPos(3) > 0) {

            team.setNumPosDown(3);
            ch = true;

        } else if ((pos.equals("1B") || pos.equals("3B")) && team.getNumPos(2) > 0) {
            team.setNumPosDown(2);
            ch = true;
        } else if (pos.equals("2B") && team.getNumPos(4) > 0) {

            team.setNumPosDown(4);
            ch = true;

        } else if (pos.equals("SS") && team.getNumPos(6) > 0) {

            team.setNumPosDown(6);
            ch = true;

        } else if ((pos.equals("SS") || pos.equals("2B")) && team.getNumPos(5) > 0) {
            team.setNumPosDown(5);
            ch = true;
        } else if (pos.equals("OF") && team.getNumPos(8) > 0) {
            team.setNumPosDown(8);
            ch = true;
        }

        if (!ch && team.getNumPos(7) > 0) {
            team.setNumPosDown(7);
        }

        System.out.println();
        for (int i = 0; i < 10; i++) {
            System.out.println("i : " + i + " " + team.getNumPos(i));
        }

    }
}
