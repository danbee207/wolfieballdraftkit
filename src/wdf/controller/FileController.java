package wdf.controller;

import java.io.File;
import java.io.IOException;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import wdf.WDK_PropertyType;
import static wdf.WDK_PropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static wdf.WDK_StartupConstants.PATH_DRAFT;
import wdf.data.Draft;
import wdf.error.FileErrorHandler;
import wdf.file.DraftSiteExporter;
import wdf.file.WolfieballFileManager;
import wdf.gui.MessageDialog;
import wdf.gui.ProgressDialog;
import wdf.gui.WDK_GUI;
import wdf.gui.YesNoCancelDialog;
import wdf.manager.DraftDataManager;

/**
 * This controller class provides responses to interactions with the buttons in
 * the top toolbar.
 *
 * @author DnB
 */
public class FileController {
    
    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE DRAFT DATA
    private WolfieballFileManager draftIO;
    
    // THIS GUY KNOWS HOW TO EXPORT DRAFT SCHEDULE PAGES
    private DraftSiteExporter exporter;
    
    // THIS WILL PROVIDE FEEDBACK TO THE USER WHEN SOMETHING GOES WRONG
    FileErrorHandler errorHandler;
    
    //THIS WILL PROVIDE FEEDBACK TO THE USER AFTER
    //WORK BY THIS CLASS HAS COMPLETED
    MessageDialog messageDialog;
    
    //AND WE'LL USE THIS TO ASK YES/NO/CANCEL/ QUESTION
    YesNoCancelDialog yesNoCancelDialog;
    
    // WE'LL USE THIS TO SHOW PROGRESS DURING EXPORTING
    ProgressDialog progressDialog;
    
    // WE'LL USE THIS TO GET OUR VERIFICATION FEEDBACK
    PropertiesManager properties;

    
    /**
     * This default constructor starts the program without a draft file being
     * edited.
     *
     * @param initDraftIO The object that will be reading and writing course
     * data.
     * @param initExporter The object that will be exporting courses to Web
     * sites.
     */
    public FileController(MessageDialog initMessageDialog,YesNoCancelDialog initYesNoCancelDialog,ProgressDialog initProgressDialog, WolfieballFileManager initDraftIO,DraftSiteExporter initExporter){
        
        
        //NOTHING YET
        saved = true;
        
        //KEEP THESE GUYS FOR LATER
        draftIO = initDraftIO;
        exporter = initExporter;
        
        //BE READY FOR ERRORS
        errorHandler = FileErrorHandler.getErrorHandler();
        
        //AND GET READY TO PROVIDE FEEDBACK
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        progressDialog = initProgressDialog;
        properties = PropertiesManager.getPropertiesManager();
    }
    
    /**
      * This method marks the appropriate variable such that we know
     * that the current Draft has been edited since it's been saved.
     * The UI is then updated to reflect this.
     * 
     * @param gui The user interface editing the Draft.
     */
    
    public void markAsEdited(WDK_GUI gui){
        
        //The Draft OBJECT is now dirty
        saved = false;
        
        //LET THE UI KNOW
        gui.updateTopToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new Draft. If a course is
     * already being edited, it will prompt the user to save it first.
     * 
     * @param gui The user interface editing the Draft.
   
     */
    public void handleNewDraftRequest(WDK_GUI gui) {

        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW DRAFT
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                DraftDataManager dataManager = gui.getDataManager();
                dataManager.reset();
                
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                gui.updateTopToolbarControls(saved);

                // TELL THE USER THE COURSE HAS BEEN CREATED
                messageDialog.show(properties.getProperty(WDK_PropertyType.NEW_DRAFT_CREATED_MESSAGE));
                
               // gui.setTeamSpace(new TeamsScreen(gui));
              ///  gui.setDraftSpace(new DraftScreen(gui));
              //  gui.setPlayerSpace(new PlayerScreen(gui));
             //   gui.setMlbSpace(new MLBScreen(gui));
             //   gui.setStandingSpace(new StandingScreen(gui));
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG, PROVIDE FEEDBACK
            errorHandler.handleNewCourseError();
        }
        
    }
    
    /**
     * This method lets the user open a Draft saved to a file. It will also
     * make sure data for the current Draft is not lost.
     * 
     * @param gui The user interface editing the Draft.
     */
    public void handleLoadDraftRequest(WDK_GUI gui) {
        
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO OPEN A Course
            if (continueToOpen) {
                // GO AHEAD AND PROCEED LOADING A Course
                promptToOpen(gui);
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG
            errorHandler.handleLoadCourseError();
        }
    
    }

    public void handleSaveDraftRequest(WDK_GUI gui, Draft draftToSave,String nameDraft) {
        try{
            System.out.println("1");
           draftIO.saveDraft(draftToSave,nameDraft);
            System.out.println("2");
           saved =true;
           System.out.println("3");
           messageDialog.show("SAVE IT!");
           System.out.println("4");
           gui.updateTopToolbarControls(saved);
           System.out.println("5");
        }catch(IOException ioe){
            ioe.printStackTrace();
            errorHandler.handleSaveDraftError();
        }
    
    }

    public void handleSaveAsDraftRequest(WDK_GUI gui,Draft draftToSAve,String nameDraft) {
        handleSaveDraftRequest(gui, draftToSAve,nameDraft);
    }

    public void handleExportDraftRequest(WDK_GUI gui) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     * 
     * @param gui 
     */
    public void handleExitDraftRequest(WDK_GUI gui) {

        try{
            //WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if(!saved){
                //THE USE CAN OPT OUR HERE
                continueToExit = promptToSave(gui);
            }
            
            //IF THE USER REALLY WANTS TO EXIT THE APP
            if(continueToExit){
                //EXIT THE APPLICATION
                System.exit(0);
            }
        }catch(IOException ioe){
            errorHandler.handleExitError();
            
        }
        
        
    }
    
    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * Draft, or opening another Draft. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave(WDK_GUI gui) throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(properties.getProperty(SAVE_UNSAVED_WORK_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            // SAVE THE COURSE
            DraftDataManager dataManager = gui.getDataManager();
            draftIO.saveDraft(dataManager.getDraft(),"");
            saved = true;
            

        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    
    
    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen(WDK_GUI gui) {
        // AND NOW ASK THE USER FOR THE DRAFT TO OPEN
        FileChooser courseFileChooser = new FileChooser();
        courseFileChooser.setInitialDirectory(new File(PATH_DRAFT));
        File selectedFile = courseFileChooser.showOpenDialog(gui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                Draft draftToLoad = gui.getDataManager().getDraft();
                draftIO.loadDraft(draftToLoad, selectedFile.getAbsolutePath());
                String[] sele = selectedFile.getPath().split("/");
            
                gui.reloadDraft(draftToLoad);
                saved = true;
                gui.updateTopToolbarControls(saved);
           
                
            } catch (Exception e) {
                FileErrorHandler eH = FileErrorHandler.getErrorHandler();
                eH.handleLoadDraftError();
            }
        }
    }
    
    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the draft is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current draft has been saved
     * since it was last edited.
     *
     * @return true if the current draft is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }

    public void handleSaveDraftRequest(Draft draft, String saveName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
