/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.controller;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Toggle;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdf.WDK_PropertyType;
import static wdf.WDK_PropertyType.*;
import wdf.data.Draft;
import wdf.data.EstimatedValueComparator;

import wdf.data.Player;
import wdf.data.Team;
import wdf.data.TeamMember;
import wdf.gui.MessageDialog;
import wdf.gui.PlayerAddDialog;
import wdf.gui.PlayerEditDialog;

import wdf.gui.PlayerScreen;
import wdf.gui.TeamsScreen;
import wdf.gui.WDK_GUI;
import wdf.gui.YesNoCancelDialog;
import wdf.manager.PlayerDataManager;

/**
 *
 * @author DnB
 */
public class PlayerEditController {

    PlayerAddDialog pad;
    PlayerEditDialog ped;
    MessageDialog msd;
    YesNoCancelDialog yncd;
    WDK_GUI wdk_gui;
    ObservableList<TeamMember> showatTable;
    Stage primaryStage;

    public PlayerEditController(Stage initPrimaryStage, MessageDialog initMessageDialog, YesNoCancelDialog inityesnodialog, WDK_GUI initwdkgui) {

        primaryStage = initPrimaryStage;

        msd = initMessageDialog;
        yncd = inityesnodialog;

        wdk_gui = initwdkgui;
        // showatTable;
    }

    public void handleRadioArrangements(PlayerScreen gui, Toggle nToggle, String searchText) {

        setTableList(nToggle, gui);
        setTableListincludingTxt(searchText, gui);

    }

    private void setTableListincludingTxt(String searchText, PlayerScreen gui) {
        if (searchText.length() == 0) {
            gui.getItemsTable().setItems(showatTable);
        } else {

            ObservableList<TeamMember> searchedList = FXCollections.observableArrayList();

            for (TeamMember p : showatTable) {
                if (searchItem(searchText, p.getLast()) || searchItem(searchText, p.getFirst())) {
                    searchedList.add(p);
                }

            }

            gui.getItemsTable().setItems(searchedList);
        }
    }

    private void setTableList(Toggle nToggle, PlayerScreen gui) {
        // ObservableList<Player> showatTable = FXCollections.observableArrayList();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ObservableList<TeamMember> members = gui.getDdataManager().getDraft().freeagentsList();
        if (nToggle.getUserData().toString().equals(props.getProperty(RADIO_ALL.toString()))) {
            showatTable = FXCollections.observableArrayList();
            for (TeamMember tMem : members) {
                showatTable.add(tMem);
            }

            changeColumnText(gui, "All");
        } else if (nToggle.getUserData().toString().equals(props.getProperty(RADIO_P.toString()))) {
            showatTable = FXCollections.observableArrayList();
            showatTable = sortedTypeTable(showatTable, members, props.getProperty(RADIO_P.toString()));
            changeColumnText(gui, "P");
        } else if (nToggle.getUserData().toString().equals(props.getProperty(RADIO_U.toString()))) {
            showatTable = FXCollections.observableArrayList();
            showatTable = hitterGroupList(showatTable, members);
            changeColumnText(gui, "");
        } else if (nToggle.getUserData().toString().equals(props.getProperty(RADIO_MI.toString()))) {
            showatTable = FXCollections.observableArrayList();
            showatTable = sortedTypeTable(showatTable, members, props.getProperty(RADIO_2B.toString()));
            showatTable = sortedTypeTable(showatTable, members, props.getProperty(RADIO_SS.toString()));
            changeColumnText(gui, "");
        } else if (nToggle.getUserData().toString().equals(props.getProperty(RADIO_CI.toString()))) {
            showatTable = FXCollections.observableArrayList();
            showatTable = sortedTypeTable(showatTable, members, props.getProperty(RADIO_1B.toString()));
            showatTable = sortedTypeTable(showatTable, members, props.getProperty(RADIO_3B.toString()));
            changeColumnText(gui, "");
        } else {
            showatTable = FXCollections.observableArrayList();
            showatTable = sortedTypeTable(showatTable, members, nToggle.getUserData().toString());
            changeColumnText(gui, "");
        }

    }

    private ObservableList<TeamMember> hitterGroupList(ObservableList<TeamMember> sortedList, ObservableList<TeamMember> players) {

        for (TeamMember p : players) {
            if (p.isHitter()) {
                sortedList.add(p);
            }
        }

        return sortedList;
    }

    private ObservableList<TeamMember> sortedTypeTable(ObservableList<TeamMember> sortedList, ObservableList<TeamMember> players, String type) {

        for (TeamMember p : players) {

            String positions = p.getQp();

            if (checkSametype(type, positions)) {
                sortedList.add(p);
            }
        }

        return sortedList;
    }

    private boolean checkSametype(String type, String qp) {

        boolean ch = false;

        if (type.length() == 1) {
            for (int i = 0; i < qp.length(); i++) {
                if (qp.charAt(i) == type.charAt(0)) {
                    return true;
                }
            }
        } else {
            for (int i = 1; i < qp.length(); i++) {
                if ((qp.charAt(i - 1) == type.charAt(0)) && (qp.charAt(i) == type.charAt(1))) {
                    return true;
                }

            }
        }
        return false;

    }

    /**
     * CHANGE COLUMN HEADER TEXT
     *
     * @param gui
     * @param type WHICH IS CHOOSED AT RADIO BUTTON
     */
    private void changeColumnText(PlayerScreen gui, String type) {

        if (type.equals("All")) {
            setColumnHeader(gui, COL_BAWHIP, COL_HRSV, COL_RBIK, COL_RW, COL_SBERA);

        } else if (type.equals("P")) {
            setColumnHeader(gui, COL_WHIP, COL_SV, COL_K, COL_W, COL_ERA);
        } else {
            setColumnHeader(gui, COL_BA, COL_HR, COL_RBI, COL_R, COL_SB);
        }
    }

    private void setColumnHeader(PlayerScreen gui, WDK_PropertyType bawhip, WDK_PropertyType hrsv, WDK_PropertyType rbik, WDK_PropertyType rw, WDK_PropertyType sbera) {

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        gui.getItemBA_WHIPColumn().setText(props.getProperty(bawhip.toString()));
        gui.getItemHR_SVColumn().setText(props.getProperty(hrsv.toString()));
        gui.getItemRBI_KColumn().setText(props.getProperty(rbik.toString()));
        gui.getItemR_WColumn().setText(props.getProperty(rw.toString()));
        gui.getItemSB_ERAColumn().setText(props.getProperty(sbera.toString()));
    }

    public void handleSearchRequest(PlayerScreen gui, PlayerDataManager pManager, String newValue) {

        ObservableList<TeamMember> searchedItem = FXCollections.observableArrayList();
        //   ObservableList<Player> allplayers = pManager.getCurrentShowPlayers();
        if (showatTable == null) {
            showatTable = gui.getDdataManager().getDraft().freeagentsList();
        }

        if (checkValue(newValue) && newValue.length() > 0) {
            for (TeamMember p : showatTable) {

                if (searchItem(newValue, p.getLast())) {

                    searchedItem.add(p);

                }

                if (searchItem(newValue, p.getFirst())) {
                    searchedItem.add(p);
                }
            }

            gui.getItemsTable().setItems(searchedItem);
        } else if (newValue.length() == 0) {
            gui.getItemsTable().setItems(showatTable);
        }

    }

    private boolean searchItem(String newValue, String name) {

        boolean ch = false;

        for (int i = 0; i < newValue.length(); i++) {
            if (name.length() > i) {

                if (newValue.toLowerCase().charAt(i) == name.toLowerCase().charAt(i)) {
                    ch = true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        return ch;

    }

    private boolean checkValue(String newValue) {

        char[] checkingValue = newValue.toCharArray();

        for (char k : checkingValue) {

            if (k >= '0' && k <= '9') {
                return false;
            }

        }

        return true;
    }

    public void handleNewPlayerRequest(PlayerScreen gui) {

        pad = new PlayerAddDialog(primaryStage, msd);
        PlayerDataManager pdm = gui.getPdataManager();
        Draft draft = gui.getDdataManager().getDraft();
        pad.showAddPlayerDailog();
        if (pad.wasCompleteSelected()) {
            Player p = pad.getPlayer();

            if (p.isHitter()) {
                gui.getDdataManager().getDraft().getHitters().add(p);
            } else {
                gui.getDdataManager().getDraft().getPitchers().add(p);
            }
            gui.getDdataManager().getDraft().getAllPlayers().add(p);

            p.setSb_era();
            TeamMember tMember = new TeamMember(p);
            eventAddListener(gui);
            gui.getDdataManager().getDraft().getFreeagent().addPlayer(tMember);

            //   gui.getItemsTable().getItems().add(p);
            wdk_gui.getFileController().markAsEdited(wdk_gui);

        } else {

        }

    }

    public void eventAddListener(PlayerScreen gui) {
        gui.getDdataManager().getDraft().freeagentsList().addListener(new ListChangeListener<TeamMember>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends TeamMember> c) {

                setTableList(gui.getRadioGrup().getSelectedToggle(), gui);
                setTableListincludingTxt(gui.getSearchTxtField().textProperty().getValue(), gui);
                /*
                 ObservableList<TeamMember> freeagent = gui.getDdataManager().getDraft().freeagentsList();
                 for (TeamMember t : freeagent) {
                 t.setEstimatedValue(1.0);
                 }
                 Collections.sort(freeagent, new EstimatedValueComparator());
                
                 for (TeamMember t : freeagent) {
                 t.setEstimatedValue(gui.getDdataManager().getDraft().getRemaingMoney()/t.getEstimatedValue());
                 }
                 */

            }
        });

    }

    public void eventAddListener(TeamsScreen gui) {
        gui.getDdataManager().getDraft().freeagentsList().addListener(new ListChangeListener<TeamMember>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends TeamMember> c) {

                HashMap<String, Team> teamMap = gui.getDdataManager().getDraft().getTeamsFromMappings();
                Team team = teamMap.get(gui.getTeamComboBox().getSelectionModel().getSelectedItem());
                ObservableList<TeamMember> teamMember = team.getTeamMember();
                gui.getLineupTable().setItems(teamMember);
                /*
                 ObservableList<TeamMember> freeagent = gui.getDdataManager().getDraft().freeagentsList();
                 for (TeamMember t : freeagent) {
                 t.setEstimatedValue(1.0);
                 }
                 Collections.sort(freeagent, new EstimatedValueComparator());
                 for (TeamMember t : freeagent) {

                 t.setEstimatedValue(gui.getDdataManager().getDraft().getRemaingMoney() / t.getEstimatedValue());
                 }
                 */

            }
        });

    }

    public void eventTaxiAddListener(TeamsScreen gui) {
        gui.getDdataManager().getDraft().freeagentsList().addListener(new ListChangeListener<TeamMember>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends TeamMember> c) {
                HashMap<String, Team> teamMap = gui.getDdataManager().getDraft().getTeamsFromMappings();
                Team team = teamMap.get(gui.getTeamComboBox().getSelectionModel().getSelectedItem());
                ObservableList<TeamMember> teamMember = team.getTaxiMember();
                gui.getTaxiTable().setItems(teamMember);

            }
        });

    }

    public void handleRemovePlayerRequest(PlayerScreen gui, TeamMember itemToRemove) {
        eventAddListener(gui);
        yncd.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_PLAYER_MESSAGE));

        //and NOWW GET THE USER'S SELECTION
        String selection = yncd.getSelection();

        //IF THE USER SAID YES, THEN REMOVE IT
        if (selection.equals(YesNoCancelDialog.YES)) {

            Team team = gui.getDdataManager().getDraft().getFreeagent();

            team.removePlayer(itemToRemove);

            wdk_gui.getFileController().markAsEdited(wdk_gui);
        }
    }

    public void handleEditPlayerRequest(PlayerScreen gui, TeamMember player) {

        eventAddListener(gui);

        Draft draft = gui.getDdataManager().getDraft();
        ped = new PlayerEditDialog(primaryStage, msd);
        ped.showEditPlayerDialog(player, gui.getDdataManager().getDraft());
        if (ped.wasCompleteSelected()) {

            HashMap<String, Team> teamMap = draft.getTeamsFromMappings();

            TeamMember teamMember = ped.getMember();

            //add taxi member to taxiMembers
            Team team = teamMap.get(teamMember.getFantasyTean());

            player.setPostion(teamMember.getPosition());
            player.setContract(teamMember.getContract());
            player.setSalary(teamMember.getSalary());
            player.setFantasyTeam(teamMember.getFantasyTean());

            if (player.getContract().equals("X")) {

                team.addNewTaxi(player);
                player.setPostion("H");
                player.setSalary(0);

            } else {
                //position
                setnumLeftPlayerPosDow(player.getPosition(), team);
                //salary
                team.setSalaryMinus(player.getSalary());

                team.addPlayer(player);

            }

            draft.getFreeagent().removePlayer(player);
            draft.addDrafted(player);

            setEValuefreagent(draft);
            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            wdk_gui.getFileController().markAsEdited(wdk_gui);
        } else {

        }
    }

    private void setEValuefreagent(Draft draft) {
        for (TeamMember t : draft.freeagentsList()) {
            t.setEstimatedValue(1.0);
        }
        Collections.sort(draft.freeagentsList(), new EstimatedValueComparator());
        for (TeamMember t : draft.freeagentsList()) {
            double result = draft.getRemaingMoney() *5/ t.getEstimatedValue();
            t.setEstimatedValue(result);
        }
    }

    public void handleEditPlayerRequest(TeamsScreen gui, TeamMember player) {

        eventAddListener(gui);

        Draft draft = gui.getDdataManager().getDraft();
        ped = new PlayerEditDialog(primaryStage, msd);
        ped.showEditPlayerDialog(player, gui.getDdataManager().getDraft());
        if (ped.wasCompleteSelected()) {

            HashMap<String, Team> teamMap = draft.getTeamsFromMappings();

            TeamMember teamMember = ped.getMember();

            Team team = teamMap.get(player.getFantasyTean());
            Team newTeam = teamMap.get(teamMember.getFantasyTean());

            System.out.println("team : " + player.getFantasyTean());
            System.out.println("New Team : " + teamMember.getFantasyTean());

            //position
            setnumLeftPlayerPosUp(player.getPosition(), team);
            if (team.getNumLeft() > 0) {
                for (TeamMember t : team.getTaxiMember()) {
                    t.resetFantasyTeam();
                    draft.getFreeagent().addPlayer(t);
                }

            }

            if (!teamMember.getFantasyTean().equals("FREE AGENT")) {
                setnumLeftPlayerPosDow(teamMember.getPosition(), newTeam);
            } else {
                draft.removeDrafted(player);
                setEValuefreagent(draft);
            }
            //salary
            team.setSalaryAdd(player.getSalary());
            newTeam.setSalaryMinus(teamMember.getSalary());

            //team
            team.removePlayer(player);
            newTeam.addPlayer(player);

            player.setPostion(teamMember.getPosition());
            player.setSalary(teamMember.getSalary());
            player.setContract(teamMember.getContract());
            player.setFantasyTeam(teamMember.getFantasyTean());

            if (teamMember.getContract().equals("S1")) {
                draft.removeDrafted(player);
            } else {
                if (draft.getDrafted().indexOf(player) == -1) {
                    draft.addDrafted(player);
                }
            }

            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            wdk_gui.getFileController().markAsEdited(wdk_gui);
        } else {

        }
    }

    private void setnumLeftPlayerPosUp(String pos, Team team) {

        boolean ch = false;

        if (pos.equals("P") && team.getNumPos(9) < 9) {
            team.setNumPosUp(9);
            ch = true;
        } else if (pos.equals("C") && team.getNumPos(0) < 1) {
            team.setNumPosUp(0);
            ch = true;
        } else if (pos.equals("1B") && team.getNumPos(1) < 1) {
            team.setNumPosUp(1);
            ch = true;
        } else if (pos.equals("3B") && team.getNumPos(3) < 1) {
            team.setNumPosUp(3);
            ch = true;
        } else if ((pos.equals("1B") || pos.equals("3B")) && team.getNumPos(2) < 1) {
            team.setNumPosUp(2);
            ch = true;
        } else if (pos.equals("2B") && team.getNumPos(4) < 1) {
            team.setNumPosUp(4);
            ch = true;
        } else if (pos.equals("SS") && team.getNumPos(6) < 1) {
            team.setNumPosUp(6);
            ch = true;

        } else if ((pos.equals("2B") || pos.equals("SS")) && team.getNumPos(5) < 1) {
            team.setNumPosUp(5);
            ch = true;
        } else if (pos.equals("OF") && team.getNumPos(8) < 5) {
            team.setNumPosUp(8);
            ch = true;
        }

        if (!ch && team.getNumPos(7) < 1) {
            team.setNumPosUp(7);
        }

        System.out.println("\n ----player numpos info----");
        for (int i = 0; i < 10; i++) {
            System.out.println("i : " + i + " " + team.getNumPos(i));
        }

    }

    private void setnumLeftPlayerPosDow(String pos, Team team) {

        boolean ch = false;

        if (pos.equals("P") && team.getNumPos(9) > 0) {
            team.setNumPosDown(9);
            ch = true;
        } else if (pos.equals("C") && team.getNumPos(0) > 0) {
            team.setNumPosDown(0);
            ch = true;
        } else if (pos.equals("1B") && team.getNumPos(1) > 0) {
            team.setNumPosDown(1);
            ch = true;
        } else if (pos.equals("3B") && team.getNumPos(3) > 0) {

            team.setNumPosDown(3);
            ch = true;

        } else if ((pos.equals("1B") || pos.equals("3B")) && team.getNumPos(2) > 0) {
            team.setNumPosDown(2);
            ch = true;
        } else if (pos.equals("2B") && team.getNumPos(4) > 0) {

            team.setNumPosDown(4);
            ch = true;

        } else if (pos.equals("SS") && team.getNumPos(6) > 0) {

            team.setNumPosDown(6);
            ch = true;

        } else if ((pos.equals("SS") || pos.equals("2B")) && team.getNumPos(5) > 0) {
            team.setNumPosDown(5);
            ch = true;
        } else if (pos.equals("OF") && team.getNumPos(8) > 0) {
            team.setNumPosDown(8);
            ch = true;
        }

        if (!ch && team.getNumPos(7) > 0) {
            team.setNumPosDown(7);
        }

        System.out.println();
        for (int i = 0; i < 10; i++) {
            System.out.println("i : " + i + " " + team.getNumPos(i));
        }

    }

    public void handleEditTaxiPlayerRequest(TeamsScreen gui, TeamMember player) {

        eventTaxiAddListener(gui);

        Draft draft = gui.getDdataManager().getDraft();
        ped = new PlayerEditDialog(primaryStage, msd);
        ped.showEditPlayerDialog(player, gui.getDdataManager().getDraft());
        if (ped.wasCompleteSelected()) {

            HashMap<String, Team> teamMap = draft.getTeamsFromMappings();

            TeamMember teamMember = ped.getMember();

            Team team = teamMap.get(player.getFantasyTean());
            Team newTeam = teamMap.get(teamMember.getFantasyTean());

            player.setPostion("H");
            player.setSalary(teamMember.getSalary());
            player.setContract(teamMember.getContract());
            player.setFantasyTeam(teamMember.getFantasyTean());

            //team
            team.removeTaxi(player);

            if (teamMember.getFantasyTean().equals("FREE AGENT")) {
                player.resetFantasyTeam();
                newTeam.addPlayer(player);
                draft.removeDrafted(player);
            } else {

                newTeam.addNewTaxi(player);
            }

            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            wdk_gui.getFileController().markAsEdited(wdk_gui);
        } else {

        }
    }

}
