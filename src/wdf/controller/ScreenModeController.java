/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.controller;

import javafx.scene.control.ScrollPane;
import wdf.gui.WDK_GUI;

/**
 *
 * @author DnB
 */
public class ScreenModeController {
 
        
    ScrollPane workPane;
    WDK_GUI workScreen;
    
    public ScreenModeController(ScrollPane initworkPane, WDK_GUI initgui){
        workPane = initworkPane;
        workScreen = initgui;
    }
        
    public void handlePlayerScreenRequest(){
        workPane.setContent(workScreen.getPlayerSpace());
    }
    
    public void handleTeamScreenRequest(){
        workPane.setContent(workScreen.getTeamSpace());
    }
    public void handleStandingScreenRequest(){
        workPane.setContent(workScreen.getStandingSpace());
    }
    public void handleDraftScreenRequest(){
        workPane.setContent(workScreen.getDraftSpace());
    }
    public void handleMLBScreenRequest(){
        workPane.setContent(workScreen.getMlbSpace());
    }


    
}
