/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.controller;

import java.util.Collections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wdf.WDK_PropertyType.REMOVE_TEAM_MESSAGE;
import wdf.data.Draft;
import wdf.data.EstimatedValueComparator;
import wdf.data.Team;
import wdf.data.TeamMember;
import wdf.gui.MessageDialog;
import wdf.gui.TeamDialog;
import wdf.gui.TeamsScreen;
import wdf.gui.WDK_GUI;
import wdf.gui.YesNoCancelDialog;

/**
 *
 * @author DnB
 */
public class TeamEditController {

    Stage primaryStage;
    WDK_GUI wdk_gui;
    MessageDialog msd;
    YesNoCancelDialog yncd;
    TeamDialog td;

    public TeamEditController(Stage initPrimaryStage, MessageDialog initMessageDialog, YesNoCancelDialog inityesnoCancelDialog, WDK_GUI initwdkgui) {

        primaryStage = initPrimaryStage;
        msd = initMessageDialog;
        yncd = inityesnoCancelDialog;

        wdk_gui = initwdkgui;

        td = new TeamDialog(primaryStage, initMessageDialog);

    }

    private void addListChanger(TeamsScreen gui) {

        gui.getDdataManager().getDraft().getTeams().addListener(new ListChangeListener<Team>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends Team> c) {
                System.out.println(" Team Change");
                ObservableList<TeamMember> freeagent = gui.getDdataManager().getDraft().freeagentsList();
                for (TeamMember t : freeagent) {
                    t.setEstimatedValue(1.0);
                }
                Collections.sort(freeagent, new EstimatedValueComparator());
                for (TeamMember t : freeagent) {

                    t.setEstimatedValue(gui.getDdataManager().getDraft().getRemaingMoney() *5/ t.getEstimatedValue());
                }
                
                

            }
        });

    }

    public Team handleNewTeamRequest(TeamsScreen gui) {

        Draft draft = gui.getDdataManager().getDraft();
        td.showAddteamDialog();
        if (td.wasCompleteSelected()) {
            addListChanger(gui);
            Team t = td.getTeam();

            draft.getTeams().add(t);

            wdk_gui.getFileController().markAsEdited(wdk_gui);

            return t;
        }

        return null;
    }

    public Team handleRemoveTeamRequest(TeamsScreen gui, Team itemToRemove) {

        yncd.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_TEAM_MESSAGE));

        //AND NOW GET THE USER'S SELECTION
        String selection = yncd.getSelection();

        //IF THE USER SAID YES, THEN REMOVE IT
        if (selection.equals(YesNoCancelDialog.YES)) {
            addListChanger(gui);

            gui.getDdataManager().getDraft().getTeams().remove(itemToRemove);
            for (TeamMember t : itemToRemove.getTeamMember()) {
                t.resetFantasyTeam();
                gui.getDdataManager().getDraft().getFreeagent().getTeamMember().add(t);
            }
            for (TeamMember t : itemToRemove.getTaxiMember()) {
                t.resetFantasyTeam();
                gui.getDdataManager().getDraft().getFreeagent().getTeamMember().add(t);
            }

            wdk_gui.getFileController().markAsEdited(wdk_gui);
            return itemToRemove;
        }

        return null;

    }

    public Team handleEditTeamRequest(TeamsScreen gui, Team itemToEdit) {

        td.showEditTeamDialog(itemToEdit);

        if (td.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM

            Team team = td.getTeam();
            itemToEdit.setName(team.getName());
            itemToEdit.setOwner(team.getOwner());

            // THE COURSE IS NOW DIRTY, MEANING IT'S BEEN 
            // CHANGED SINCE IT WAS LAST SAVED, SO MAKE SURE
            // THE SAVE BUTTON IS ENABLED
            wdk_gui.getFileController().markAsEdited(wdk_gui);

            return itemToEdit;
        } else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING

        }

        return itemToEdit;

    }

}
