/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.data;

import java.util.Comparator;

/**
 *
 * @author DnB
 */
public class ComparableTotalPoints implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        
        Team t1 = (Team)o1;
        Team t2 = (Team)o2;
        
        //R
        if(t1.getTotalR()>t2.getTotalR())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalR()==t2.getTotalR()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        
        //HR
        if(t1.getTotalHR()>t2.getTotalHR())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalHR()==t2.getTotalHR()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //RBI
        if(t1.getTotalRBI()>t2.getTotalRBI())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalRBI()==t2.getTotalRBI()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //SB
        if(t1.getTotalSB()>t2.getTotalSB())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalSB()==t2.getTotalSB()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //BA
        if(t1.getTotalBA()>t2.getTotalBA())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalBA()==t2.getTotalBA()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //W
        if(t1.getTotalW()>t2.getTotalW())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalW()==t2.getTotalW()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //SV
        if(t1.getTotalSV()>t2.getTotalSV())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalSV()==t2.getTotalSV()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //K
        if(t1.getTotalK()>t2.getTotalK())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalK()==t2.getTotalK()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //ERA
        if(t1.getTotalERA()>t2.getTotalERA())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalERA()==t2.getTotalERA()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        //WHIP
        if(t1.getTotalWHIP()>t2.getTotalWHIP())
            t1.setTotalPoints(t1.getTotalPoints()+1);
        else if(t1.getTotalWHIP()==t2.getTotalWHIP()){
            t1.setTotalPoints(t1.getTotalPoints()+1);
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
        else{
            t2.setTotalPoints(t2.getTotalPoints()+1);
        }
            
        
        return t1.getName().compareTo(t2.getName());
    }
    
}
