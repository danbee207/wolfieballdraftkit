/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.data;

import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author DnB
 */
public class Draft {
    
    String saveToName;
    ObservableList<Player> allPlayers,hitters,pitchers;
    ObservableList<TeamMember> drafted;
    ObservableList<Team> teams;
    Team freeagent;
 //   ObservableList<TeamMember> freeagents;
    
    public Draft(ObservableList<Player> inithitters, ObservableList<Player> initpitchers,ObservableList<Player> initall){
    
        saveToName = "";
        allPlayers = initall;
        
        
        hitters = inithitters;
        pitchers = initpitchers;
        
        teams = FXCollections.observableArrayList();
        drafted = FXCollections.observableArrayList();
        freeagent = new Team();
        freeagent.setName("FREE AGENT");
        freeagent.numPosReset();
        
        for(Player p : allPlayers){
            TeamMember t = new TeamMember(p);
            freeagent.getTeamMember().add(t);
        }
        
        teams.add(freeagent);
        System.out.println("team has free agent : "+ teams.size());
    }

    public String getSaveToName(){
        return saveToName;
    }
    public void setSaveToName(String name){
        saveToName=name;
    }
    public Team getFreeagent(){
        return freeagent;
    }

    public ObservableList<Player> getAllPlayers() {
        return allPlayers;
    }

    public void setAllPlayers(ObservableList<Player> allPlayers) {
        this.allPlayers = allPlayers;
    }

    public ObservableList<Player> getHitters() {
        return hitters;
    }

    public void setHitters(ObservableList<Player> hitters) {
        this.hitters = hitters;
    }

    public ObservableList<Player> getPitchers() {
        return pitchers;
    }

    public void setPitchers(ObservableList<Player> pitchers) {
        this.pitchers = pitchers;
    }

    public ObservableList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ObservableList<Team> teams) {
        this.teams = teams;
    }
    
    public TeamMember getFreeagents(int index){
        return this.freeagent.getTeamMember().get(index);
                
    }
    public ObservableList<TeamMember> freeagentsList(){
        return this.freeagent.teamMember;
    }
    
    public HashMap<String,Team> getTeamsFromMappings(){
        HashMap<String,Team> teamMap = new HashMap();
        
        for(Team team: teams ){
            teamMap.put(team.getName(), team);
        }
        return teamMap;
    }
    
    public boolean isTaxiPossible(){
        
        boolean ch=true;
        for(Team team: teams){
            if(team.numLeftPeople()>0){
                return false;
            }
            
        }
        
        return ch;
        
        
    }
    public int getRemaingMoney(){
        int remaining =0;
        
        for(int i=1;i<teams.size();i++){
            remaining += teams.get(i).getSalary();
        }
        
        return remaining;
            
    }

    public ObservableList<TeamMember> getDrafted() {
        return drafted;
    }
    
    public void addDrafted(TeamMember member){
        drafted.add(member);
    }
    public void removeDrafted(TeamMember member){
        drafted.remove(member);
    }

    public void setDrafted(ObservableList<TeamMember> drafted) {
        this.drafted = drafted;
    }
    
    public HashMap<String,Player> getPlayerHashMap(){
        HashMap<String,Player> playerMap = new HashMap();
     
        for(Player player: allPlayers ){
            playerMap.put(player.getLast(), player);
        }
        return playerMap;
        
    }
}
