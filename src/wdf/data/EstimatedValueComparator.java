/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.data;

import java.util.Comparator;

/**
 *
 * @author DnB
 */
public class EstimatedValueComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        TeamMember t1 = (TeamMember)o1;
        TeamMember t2 = (TeamMember)o2;
        
        double t1Evalue=0;
        double t2Evalue=0;
        
        //hitter comparator
        if((t1.isHitter() && t2.isHitter()) || (!t1.isHitter() && !t2.isHitter())){
        
            //r
            if(t1.getW_r()>t2.getW_r())
               t2Evalue+=1;
            else if(t1.getW_r()==t2.getW_r()){
                t1Evalue+=1;
                t2Evalue+=1;
            }else
                t1Evalue+=1;
            //hr
            if(t1.getHr_sv()>t2.getHr_sv())
                t2Evalue+=1;
            else if(t1.getHr_sv()==t2.getHr_sv()){
                t1Evalue+=1;
                t2Evalue+=1;
            }else
                t1Evalue+=1;
            //rbi
            if(t1.getK_rbi()>t2.getK_rbi())
                t2Evalue+=1;
            else if(t1.getK_rbi()==t2.getK_rbi()){
                t1Evalue+=1;
                t2Evalue+=1;
            }else
                t1Evalue+=1;
            //sb
            if(Double.parseDouble(t1.getSb_era())>Double.parseDouble(t2.getSb_era()))
                t2Evalue+=1;
            else if(Double.parseDouble(t1.getSb_era())==Double.parseDouble(t2.getSb_era())){
                t1Evalue+=1;
                t2Evalue+=1;
            }else
                t1Evalue+=1;
            //ba
            if(t1.getBa_whip()>t2.getBa_whip())
                t2Evalue+=1;
            else if(t1.getBa_whip()==t2.getBa_whip()){
                t1Evalue+=1;
                t2Evalue+=1;
            }else
                t1Evalue+=1;
        }
        
        t1.setEstimatedValue(t1.getEstimatedValue()+t1Evalue);
        t2.setEstimatedValue(t2.getEstimatedValue()+t2Evalue);

        return t1.getFirst().compareTo(t2.getFirst());
        
    
    }
    
}
