/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DnB
 */
public class Player implements Comparable{
    
    final StringProperty first,last, qp, team,notes,nation,sb_era,ba_whipString;
   // final IntegerProperty ab_er,h,w_r,bb_rbi,hr_sv,estimatedValue,birth; 
    final IntegerProperty w_r,hr_sv,h,k_rbi,ab_er,sb_bb,birth;
    final DoubleProperty ip,era,ba_whip,estimatedValue;
    
    private boolean hitter;
    public static final String DEFAULT_STRING = "";
    public static final String DEFAULT_PITCHER = "P";
    public static final int DEFAULT_INTEGER =0;
    public static final double DEFAULT_DOUBLE=0.0;

    public Player(){
        hitter=true;
        first = new SimpleStringProperty(DEFAULT_STRING);
        last  = new SimpleStringProperty(DEFAULT_STRING);
        team = new SimpleStringProperty(DEFAULT_STRING);
        qp = new SimpleStringProperty(DEFAULT_STRING);
        notes = new SimpleStringProperty(DEFAULT_STRING);
        nation = new SimpleStringProperty(DEFAULT_STRING);
        sb_era = new SimpleStringProperty(DEFAULT_STRING);
        ba_whipString = new SimpleStringProperty(DEFAULT_STRING);
        
        w_r = new SimpleIntegerProperty(DEFAULT_INTEGER);
        hr_sv = new SimpleIntegerProperty(DEFAULT_INTEGER);
        k_rbi = new SimpleIntegerProperty(DEFAULT_INTEGER);
        ab_er = new SimpleIntegerProperty(DEFAULT_INTEGER);
        sb_bb = new SimpleIntegerProperty(DEFAULT_INTEGER);
       
        birth = new SimpleIntegerProperty(DEFAULT_INTEGER);
        h = new SimpleIntegerProperty(DEFAULT_INTEGER);
        
        ip = new SimpleDoubleProperty(DEFAULT_DOUBLE);
        era = new SimpleDoubleProperty(DEFAULT_DOUBLE);
        ba_whip = new SimpleDoubleProperty(DEFAULT_DOUBLE);
        estimatedValue = new SimpleDoubleProperty(DEFAULT_DOUBLE);
    }
    
    
    public Player(String initfirst,String initlast,String initteam,String initnotes,String initnation, String initqp,int initbirth, int initab_er,int initk_rbi,int inithr_sv, int inith, int initw_r,double initip,int initsb_bb,boolean hitter){
        
        this.hitter=hitter;
        
        
        first = new SimpleStringProperty(initfirst);
        last = new SimpleStringProperty(initlast);
        team = new SimpleStringProperty(initteam);
        if(hitter){
            qp = new SimpleStringProperty(initqp);
            ip = new SimpleDoubleProperty(DEFAULT_DOUBLE);
            if(initab_er>0)
                ba_whip = new SimpleDoubleProperty((double)inith/initab_er);
            else
                ba_whip = new SimpleDoubleProperty(0);
            
            ba_whipString = new SimpleStringProperty(String.format("%.3f", ba_whip.get()));
            
            era = new SimpleDoubleProperty(DEFAULT_DOUBLE);
            sb_era= new SimpleStringProperty(Integer.toString(initsb_bb));
            
        }
        else{
            qp = new SimpleStringProperty(DEFAULT_PITCHER);
            ip = new SimpleDoubleProperty(initip);         
            if(initip>0){
            ba_whip = new SimpleDoubleProperty((double)(initw_r+inith)/initip);
            era = new SimpleDoubleProperty((double)initab_er*9/initip);
           
            }
            else{
                ba_whip = new SimpleDoubleProperty(0);
                era=new SimpleDoubleProperty(0);
            }
            ba_whipString = new SimpleStringProperty(String.format("%.2f", ba_whip.get()));
            sb_era= new SimpleStringProperty(String.format("%.2f", era.get()));
        }
            
        notes = new SimpleStringProperty(initnotes);
        nation = new SimpleStringProperty(initnation);
        
        birth = new SimpleIntegerProperty(initbirth);       
        ab_er = new SimpleIntegerProperty(initab_er);
        k_rbi = new SimpleIntegerProperty(initk_rbi);
        hr_sv = new SimpleIntegerProperty(inithr_sv);
        
        h = new SimpleIntegerProperty(inith);
        w_r = new SimpleIntegerProperty(initw_r);
        sb_bb = new SimpleIntegerProperty(initsb_bb);
        
        estimatedValue = new SimpleDoubleProperty(DEFAULT_DOUBLE);
        
        
        
             
    }

    public void setHitter(boolean hitter) {
        this.hitter = hitter;
    }
    
    
    
    public void reset(){
        setFirst(DEFAULT_STRING);
        setLast(DEFAULT_STRING);
        setQp(DEFAULT_STRING);
        setTeam(DEFAULT_STRING);
        setNotes(DEFAULT_STRING);
        setNation(DEFAULT_STRING);
        
        setBirth(DEFAULT_INTEGER);
        setW_r(DEFAULT_INTEGER);
        setHr_sv(DEFAULT_INTEGER);
        setK_rbi(DEFAULT_INTEGER);
        setH(DEFAULT_INTEGER);
        setIp(DEFAULT_INTEGER);
        setAb_er(DEFAULT_INTEGER);
        setEstimatedValue(DEFAULT_INTEGER);
        setBa_whip(DEFAULT_DOUBLE);
        setSb_bb(DEFAULT_INTEGER);
        setEra(DEFAULT_DOUBLE);
        
        
    }
    
    public boolean isHitter(){
        return hitter;
    }

    public void setSb_era(){
        
        if(isHitter()){
            sb_era.set(Integer.toString(sb_bb.get()));
        }
        else{
            
            sb_era.set(String.format("%.2f", era.get()));
        }
    }
    public void setEra(double value){
        era.set(DEFAULT_DOUBLE);
    }
    public void setFirst(String initTopic){
        first.set(initTopic);
    }
    public void setLast(String initTopic){
        last.set(initTopic);
    }
    public void setTeam(String initTopic){
        team.set(initTopic);
    }
    public void setQp(String initTopic){
        qp.set(initTopic);
    }
    public void setNation(String initTopic){
        nation.set(initTopic);
    }
    public void setNotes(String initTopic){
        notes.set(initTopic);
    }
    public void setBirth(int initTopic){
        birth.set(initTopic);
    }

    public StringProperty teamProperty() {
        return team;
    }
    
    public void setW_r(int initTopic){
        w_r.set(initTopic);
    }
    public void setHr_sv(int initTopic){
        hr_sv.set(initTopic);
    }
    public void setK_rbi(int initTopic){
        k_rbi.set(initTopic);
    }
    public void setAb_er(int initTopic){
        ab_er.set(initTopic);
    }
    public void setIp(double initTopic){
        ip.set(initTopic);
    }
    public void setH(int initTopic){
        h.set(initTopic);
    }
    public void setEstimatedValue(double initTopic){
        estimatedValue.set(initTopic);
    }

    public StringProperty fristProperty(){
        return first;
    }
    
    public String getFirst() {
        return first.get();
    }

    public String getLast() {
        return last.get();
    }
    
    public StringProperty lastastProperty() {
        return last;
    }

    public StringProperty qpProperty() {
        return qp;
    }
    public String getQp() {
        return qp.get();
    }

    public String getTeam() {
        return team.get();
    }


    public String getNotes() {
        return notes.get();
    }
    public StringProperty notesProperty() {
        return notes;
    }

    public String getNation() {
        return nation.get();
    }
    public StringProperty nationProperty() {
        return nation;
    }


    public int getAb_er() {
        return ab_er.get();
    }
    public IntegerProperty ab_erProperty() {
        return ab_er;
    }

    public int getH() {
        return h.get();
    }
    public IntegerProperty hProperty() {
        return h;
    }

    public IntegerProperty w_rProperty() {
        return w_r;
    }
    public int getW_r() {
        return w_r.get();
    }
    
    public IntegerProperty k_rbiProperty() {
        return k_rbi;
    }
    public int getK_rbi() {
        return k_rbi.get();
    }
    
    public int getHr_sv() {
        return hr_sv.get();
    }
    public IntegerProperty hr_svProperty() {
        return hr_sv;
    }

    public double getEstimatedValue() {
        return estimatedValue.get();
    }
    public DoubleProperty estimatedValueProperty() {
        return estimatedValue;
    }

    public IntegerProperty birthProperty() {
        return birth;
    }
    public int getBirth() {
        return birth.get();
    }

    public double getIp() {
        return ip.get();
    }

    public DoubleProperty ipProperty() {
        return ip;
    }

    public IntegerProperty sb_bbProperty() {
        return sb_bb;
    }
    
    public int getSb_bb() {
        return sb_bb.get();
    }

    public DoubleProperty ba_whipProperty() {
        return ba_whip;
    }
    
    public double getBa_whip() {
        return ba_whip.get();
    }

    public void setSb_bb(int value){
       sb_bb.set(value);
    }
    
    public void setBa_whip(double value){
        ba_whip.set(value);
        if(isHitter())
            ba_whipString.set(String.format("%.3f", ba_whip.get()));
        else
            ba_whipString.set(String.format("%.2f", ba_whip.get()));
    }
    
    public double getEra(){
        return era.get();
    }
    public DoubleProperty eraProperty(){
        return era;
    }
    
    public StringProperty sb_eraProperty(){
        return sb_era;
    }

    public String getBa_whipString() {
        return ba_whipString.get();
    }
    
    
    public String getSb_era(){
        return sb_era.get();
    }
    
    @Override
    public int compareTo(Object o) {
        Player p = (Player)o;
        int result =getLast().compareTo(p.getLast());
        if(result ==0){
            return getFirst().compareTo(p.getFirst());
        }
        else return result;
    }
    
}
