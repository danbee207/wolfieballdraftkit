/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.data;

import com.sun.org.apache.xerces.internal.util.FeatureState;
import java.util.Collections;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author DnB
 */
public class Team {

    final StringProperty name;
    final StringProperty owner;
    final IntegerProperty salary;

    final IntegerProperty numLeft, perplay, totalPoints;
    //hitter
    final IntegerProperty totalR, totalHR, totalRBI, totalSB, totalAB, totalH_hitter;
    //pitcher
    final IntegerProperty totalW, totalSV, totalK, totalER, totalH_pitcher;
    final DoubleProperty totalBA, totalERA, totalWHIP, totalIP;

    int[] numPos; //C,num1B,numCI,num3B,num2B,numMI,numSS,numU,numOF,numP;

    ObservableList<TeamMember> teamMember;
    ObservableList<TeamMember> taxiMember;

    public static final String DEFAULT_TEAM = "";
    public static final int DEFAULT_NUM = 1;

    public static final int DEFAULT_C = 2;
    public static final int DEFAULT_1B = 1;
    public static final int DEFAULT_CI = 1;
    public static final int DEFAULT_3B = 1;
    public static final int DEFAULT_2B = 1;
    public static final int DEFAULT_MI = 1;
    public static final int DEFAULT_SS = 1;
    public static final int DEFAULT_U = 1;
    public static final int DEFAULT_OF = 5;
    public static final int DEFAULT_P = 9;

    public Team() {

        name = new SimpleStringProperty(DEFAULT_TEAM);
        owner = new SimpleStringProperty(DEFAULT_TEAM);
        salary = new SimpleIntegerProperty(260);
        numLeft = new SimpleIntegerProperty(23);
        perplay = new SimpleIntegerProperty(260/23);
        totalPoints = new SimpleIntegerProperty(0);
        //hitter
        totalR = new SimpleIntegerProperty(0);
        totalHR = new SimpleIntegerProperty(0);
        totalRBI = new SimpleIntegerProperty(0);
        totalSB = new SimpleIntegerProperty(0);
        totalAB = new SimpleIntegerProperty(0);
        totalH_hitter = new SimpleIntegerProperty(0);

        totalBA = new SimpleDoubleProperty(0);

        //pitcher
        totalW = new SimpleIntegerProperty(0);
        totalSV = new SimpleIntegerProperty(0);
        totalK = new SimpleIntegerProperty(0);
        totalER = new SimpleIntegerProperty(0);
        totalH_pitcher = new SimpleIntegerProperty(0);
        totalIP = new SimpleDoubleProperty(0);

        totalERA = new SimpleDoubleProperty(0);
        totalWHIP = new SimpleDoubleProperty(0);

        teamMember = FXCollections.observableArrayList();
        taxiMember = FXCollections.observableArrayList();

        numPos = new int[10];
        numPos[0] = DEFAULT_C;
        numPos[1] = DEFAULT_1B;
        numPos[2] = DEFAULT_CI;
        numPos[3] = DEFAULT_3B;
        numPos[4] = DEFAULT_2B;
        numPos[5] = DEFAULT_MI;
        numPos[6] = DEFAULT_SS;
        numPos[7] = DEFAULT_U;
        numPos[8] = DEFAULT_OF;
        numPos[9] = DEFAULT_P;

    }

    public void numPosReset() {
        for (int i = 0; i < numPos.length; i++) {
            numPos[i] = 0;
        }
    }

    public int getNumPos(int index) {
        return numPos[index];
    }

    public void setNumPosUp(int index) {
        numPos[index]++;
       

    }

    public void setNumPosDown(int index) {
        numPos[index]--;
        
    }

    public ObservableList<TeamMember> getTeamMember() {
        return teamMember;
    }

    public void setTeamMember(ObservableList<TeamMember> teamMember) {
        this.teamMember = teamMember;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty ownerProperty() {
        return owner;
    }

    public void setName(String initname) {
        name.set(initname);
    }

    public void setOwner(String initOwner) {
        owner.set(initOwner);
    }

    public String getName() {
        return name.get();
    }

    public String getOwner() {
        return owner.get();
    }

    public int getNumLeft() {
        numLeft.set(numLeftPeople());
        return numLeft.get();
    }

    public void setNumLeft(int initNumLeft) {
        numLeft.set(initNumLeft);
    }

    public int getPerplay() {
        return perplay.get();
    }

    public void setPerplay(int initPerplay) {
        perplay.set(initPerplay);
    }

    public int getTotalPoints() {
        return totalPoints.get();
    }

    public void setTotalPoints(int initTotalPoints) {
        totalPoints.set(initTotalPoints);
    }

    public int getTotalR() {
        return totalR.get();
    }

    public void setTotalR(int initTotalR) {
        totalR.set(initTotalR);
    }

    public int getTotalHR() {
        return totalHR.get();
    }

    public void setTotalHR(int initTotalHR) {
        totalHR.set(initTotalHR);
    }

    public int getTotalRBI() {
        return totalRBI.get();
    }

    public void setTotalRBI(int initTotalRBI) {
        totalRBI.set(initTotalRBI);
    }

    public int getTotalSB() {
        return totalSB.get();
    }

    public void setTotalSB(int initTotalSB) {
        totalSB.set(initTotalSB);
    }

    public int getTotalW() {
        return totalW.get();
    }

    public void setTotalW(int initTotalW) {
        totalW.set(initTotalW);
    }

    public int getTotalSV() {
        return totalSV.get();
    }

    public void setTotalSV(int initTotalSV) {
        totalSV.set(initTotalSV);
    }

    public int getTotalK() {
        return totalK.get();
    }

    public void setTotalK(int initTotalK) {
        totalK.set(initTotalK);
    }

    public double getTotalBA() {
        return totalBA.get();
    }

    public void setTotalBA(double initTotalBA) {
        totalBA.set(initTotalBA);
    }

    public double getTotalERA() {
        return totalERA.get();
    }

    public void setTotalERA(double initTotalERA) {
        totalERA.set(initTotalERA);
    }

    public double getTotalWHIP() {
        return totalWHIP.get();
    }

    public void setTotalWHIP(double initTotalWHIP) {
        totalWHIP.set(initTotalWHIP);
    }

    public void addPlayer(TeamMember p) {
        teamMember.add(p);

        numLeft.set(numLeftPeople());
        if (numLeft.get() != 0) {
            perplay.set(salary.get() / numLeft.get());
        } else {
            perplay.set(-1);
        }

        if (p.isHitter()) {
            totalR.set(totalR.get() + p.getW_r());
            totalHR.set(totalHR.get() + p.getHr_sv());
            totalRBI.set(totalRBI.get() + p.getK_rbi());
            totalSB.set(totalSB.get() + p.getSb_bb());
            totalH_hitter.set(totalH_hitter.get() + p.getH());
            totalAB.set(totalAB.get() + p.getAb_er());

            if (totalAB.get() != 0) {
                totalBA.set((double) totalH_hitter.get() / totalAB.get());
            }
        } else {
            totalW.set(totalW.get() + p.getW_r());
            totalSV.set(totalSV.get() + p.getHr_sv());
            totalK.set(totalK.get() + p.getK_rbi());
            totalER.set(totalER.get() + p.getAb_er());
            totalIP.set(totalIP.get() + p.getIp());
            totalH_pitcher.set(totalH_pitcher.get() + p.getH());

            if (totalIP.get() != 0) {
                totalERA.set(totalER.get() * 9 / totalIP.get());
                totalWHIP.set((totalW.get() + totalH_pitcher.get()) / totalIP.get());
            }
        }

        Collections.sort(teamMember);
    }

    public void removePlayer(TeamMember p) {
        teamMember.remove(p);

        numLeft.set(numLeftPeople());
        if (numLeft.get() != 0) {
            perplay.set(salary.get() / numLeft.get());
        } else {
            perplay.set(-1);
        }

        if (p.isHitter()) {
            totalR.set(totalR.get() - p.getW_r());
            totalHR.set(totalHR.get() - p.getHr_sv());
            totalRBI.set(totalRBI.get() - p.getK_rbi());
            totalSB.set(totalSB.get() - p.getSb_bb());
            totalH_hitter.set(totalH_hitter.get() - p.getH());
            totalAB.set(totalAB.get() - p.getAb_er());
            if (totalAB.get() != 0) {
                totalBA.set(totalH_hitter.get() / totalAB.get());
            } else {
                totalBA.set(0);
            }
        } else {
            totalW.set(totalW.get() - p.getW_r());
            totalSV.set(totalSV.get() - p.getHr_sv());
            totalK.set(totalK.get() - p.getK_rbi());
            totalER.set(totalER.get() - p.getAb_er());
            totalIP.set(totalIP.get() - p.getIp());
            totalH_pitcher.set(totalH_pitcher.get() - p.getH());

            if (totalIP.get() != 0) {
                totalERA.set(totalER.get() * 9 / totalIP.get());
                totalWHIP.set((totalW.get() + totalH_pitcher.get()) / totalIP.get());
            } else {
                totalERA.set(0);
                totalWHIP.set(0);
            }

        }

        Collections.sort(teamMember);
    }

    public int getSalary() {
        return salary.get();
    }

    public void setSalary(int initSalary) {
        salary.set(initSalary);
    }

    public void setSalaryMinus(int msalary) {
        salary.set(salary.get() - msalary);
    }

    public void addNewTaxi(TeamMember teamMember) {
        taxiMember.add(teamMember);
        teamMember.setPostion("H");
        

    }

    public int getTaxinum(){
        return taxiMember.size();
    }
    public void removeTaxi(TeamMember teamMember) {
        taxiMember.remove(teamMember);
    }

    public ObservableList<TeamMember> getTaxiMember() {
        return taxiMember;
    }

    public IntegerProperty salaryProperty() {
        return salary;
    }

    public void setSalaryAdd(int previous) {
        salary.set(salary.get() + previous);
    }

    public int numLeftPeople() {
        int numPeople = 0;
        for (int i : numPos) {
            numPeople += i;
        }

        return numPeople;
    }

}
