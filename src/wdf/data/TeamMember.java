/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author DnB
 */
public final class TeamMember extends Player implements Comparable {

    final StringProperty position, contract, fantasyTeam;
    final IntegerProperty salary;

    public static final String DEFAULT_FANTASY_TEAM = "FREE AGENT";

    public TeamMember() {

        fantasyTeam = new SimpleStringProperty(DEFAULT_FANTASY_TEAM);
        position = new SimpleStringProperty(DEFAULT_STRING);
        contract = new SimpleStringProperty(DEFAULT_STRING);
        salary = new SimpleIntegerProperty(DEFAULT_INTEGER);
    }
    
    public TeamMember(String first, String last,String fantasy,Player player){
        this.setFirst(first);
        this.setLast(last);
        fantasyTeam = new SimpleStringProperty(fantasy);
        position = new SimpleStringProperty("H");
        contract = new SimpleStringProperty("X");

        salary = new SimpleIntegerProperty(0);

        this.setQp(player.getQp());
        this.setTeam(player.getTeam());
        this.setNotes(player.getNotes());
        this.setNation(player.getNation());
        this.setHitter(player.isHitter());
        this.setIp(player.getIp());
        this.setBa_whip(player.getBa_whip());
        this.setEra(player.getEra());
        this.setSb_era(player.getSb_era());
        this.setBirth(player.getBirth());
        this.setAb_er(player.getAb_er());
        this.setK_rbi(player.getK_rbi());
        this.setHr_sv(player.getHr_sv());
        this.setH(player.getH());
        this.setW_r(player.getW_r());
        this.setSb_bb(player.getSb_bb());
        this.setEstimatedValue(player.getEstimatedValue());
    }

    public TeamMember(String first, String last, String pos, String initcontract, String fantasy, int initsalary, Player player) {
        this.setFirst(first);
        this.setLast(last);
        fantasyTeam = new SimpleStringProperty(fantasy);
        position = new SimpleStringProperty(pos);
        contract = new SimpleStringProperty(initcontract);
        salary = new SimpleIntegerProperty(initsalary);

        this.setQp(player.getQp());
        this.setTeam(player.getTeam());
        this.setNotes(player.getNotes());
        this.setNation(player.getNation());
        this.setHitter(player.isHitter());
        this.setIp(player.getIp());
        this.setBa_whip(player.getBa_whip());
        this.setEra(player.getEra());
        this.setSb_era(player.getSb_era());
        this.setBirth(player.getBirth());
        this.setAb_er(player.getAb_er());
        this.setK_rbi(player.getK_rbi());
        this.setHr_sv(player.getHr_sv());
        this.setH(player.getH());
        this.setW_r(player.getW_r());
        this.setSb_bb(player.getSb_bb());
        this.setEstimatedValue(player.getEstimatedValue());
    }

    public TeamMember(Player player) {

        this.setFirst(player.getFirst());
        this.setLast(player.getLast());
        this.setQp(player.getQp());
        this.setTeam(player.getTeam());
        this.setNotes(player.getNotes());
        this.setNation(player.getNation());
        this.setHitter(player.isHitter());
        this.setIp(player.getIp());
        this.setBa_whip(player.getBa_whip());
        this.setEra(player.getEra());
        this.setSb_era(player.getSb_era());
        this.setBirth(player.getBirth());
        this.setAb_er(player.getAb_er());
        this.setK_rbi(player.getK_rbi());
        this.setHr_sv(player.getHr_sv());
        this.setH(player.getH());
        this.setW_r(player.getW_r());
        this.setSb_bb(player.getSb_bb());
        this.setEstimatedValue(player.getEstimatedValue());

        fantasyTeam = new SimpleStringProperty(DEFAULT_FANTASY_TEAM);
        position = new SimpleStringProperty(DEFAULT_STRING);
        contract = new SimpleStringProperty(DEFAULT_STRING);

        salary = new SimpleIntegerProperty(DEFAULT_INTEGER);

    }

    public void setPlayerInfo(Player player) {

        System.out.println(player.getFirst());
        this.setFirst(player.getFirst());
        this.setLast(player.getLast());
        this.setQp(player.getQp());
        this.setTeam(player.getTeam());
        this.setNotes(player.getNotes());
        this.setNation(player.getNation());
        this.setHitter(player.isHitter());
        this.setIp(player.getIp());
        this.setBa_whip(player.getBa_whip());
        this.setEra(player.getEra());
        this.setSb_era(player.getSb_era());
        this.setBirth(player.getBirth());
        this.setAb_er(player.getAb_er());
        this.setK_rbi(player.getK_rbi());
        this.setHr_sv(player.getHr_sv());
        this.setH(player.getH());
        this.setW_r(player.getW_r());
        this.setSb_bb(player.getSb_bb());
        this.setEstimatedValue(player.getEstimatedValue());

    }

    public void setSb_era(String sbera) {
        sb_era.set(sbera);
    }

    public void setPostion(String initpos) {
        position.set(initpos);

    }

    public void setContract(String initcontract) {
        contract.set(initcontract);

    }

    public void setSalary(int initsalary) {
        salary.set(initsalary);
    }

    public void setFantasyTeam(String fantasy) {
        fantasyTeam.set(fantasy);
    }

    public String getFantasyTean() {
        return fantasyTeam.get();
    }

    public StringProperty fantasyTeamProperty() {
        return fantasyTeam;
    }

    public void resetFantasyTeam() {
        fantasyTeam.set(DEFAULT_FANTASY_TEAM);
        contract.set(DEFAULT_STRING);
        position.set(DEFAULT_STRING);
        salary.set(0);
    }

    public String getPosition() {
        return position.get();
    }

    public String getContract() {
        return contract.get();
    }

    public int getSalary() {
        return salary.get();
    }

    public StringProperty positionProperty() {
        return position;
    }

    public StringProperty contractProperty() {
        return contract;
    }

    public IntegerProperty salaryProperty() {
        return salary;
    }

    public int getPosQuality(String pos) {

        if (pos.equals("C")) {
            return 0;
        } else if (pos.equals("1B")) {
            return 1;
        } else if (pos.equals("CI")) {
            return 2;
        } else if (pos.equals("3B")) {
            return 3;
        } else if (pos.equals("2B")) {
            return 4;
        } else if (pos.equals("MI")) {
            return 5;
        } else if (pos.equals("SS")) {
            return 6;
        } else if (pos.equals("U")) {
            return 7;
        } else if (pos.equals("OF")) {
            return 8;
        } else {
            return 9;
        }

    }

    public int positionCompareTo(String first, String second) {

        if (getPosQuality(first) > getPosQuality(second)) {
            return 1;
        } else if (getPosQuality(first) == getPosQuality(second)) {
            return 0;
        } else {
            return -1;
        }

    }

    public void backToFreeAgent() {
        contract.set("");
        position.set("");
        salary.set(DEFAULT_INTEGER);
    }

    @Override
    public int compareTo(Object o) {
        TeamMember other = (TeamMember) o;

        return positionCompareTo(getPosition(), other.getPosition());

    }

}
