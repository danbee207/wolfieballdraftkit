/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.file;

/**
 * This class is responsible for exporting draft.html to its proper
 * directory. Note that it uses a base file in the baseDir directory, which gets
 * loaded first and that each draft will have its own file exported to a
 * directory in the sitesDir directory.
 *
 * @author DnB
 */
public class DraftSiteExporter {
    
    // THERE ARE A NUMBER OF CONSTANTS THAT WE'LL USE FOR FINDING
    // ELEMENTS IN THE PAGES WE'RE LOADING, AS WELL AS THINGS WE'LL
    // BUILD INTO OUR PAGE WHILE EXPORTING
    public static final String ID_NAVBAR = "navbar";
    public static final String ID_BANNER = "banner";
    
    
    // THESE ARE THE POSSIBLE SITE PAGES OUR DRAFT PAGE
    // MAY NEED TO LINK TO
    public static final String SUMMERY_PAGE = "summery.html";
    public static final String ROSTER_PAGE = "roaster.html";
    
    //THIS IS THE DIRECTORY STRUCTURE USED BY OUR SITE
    public static final String CSS_DIR = "css";
    public static final String IMAGES_DIR = "img";
    
    //AND SOME TEXT WE'LL NEED TO ADD ON THE FLY
    public static final String SLASH = "/";
    public static final String DASH = " - ";
    public static final String LINE_BREAK = "<br/>";
    
    //THSES ARE THE DIRECTORIES WHERE OUR BASE ROASTER 
    //FILE IS AND WHERE OUR COURSE SITES WILL BE EXPORTED TO
    String baseDir;
    String sitesDir;
 
    
    public DraftSiteExporter(String initBaseDir, String initSitesDir){
        baseDir = initBaseDir;
        sitesDir = initSitesDir;
    }
}
