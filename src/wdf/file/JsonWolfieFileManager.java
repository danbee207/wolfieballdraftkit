/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import wdf.WDK_StartupConstants;
import wdf.data.Draft;
import wdf.data.Player;
import wdf.data.Team;
import wdf.data.TeamMember;

/**
 * This is a WolfieballFileManager that uses the JSON file format to 
 * implement the necessary functions for loading and saving different
 * data for our player, draft.
 * 
 * @author DnB
 */
public class JsonWolfieFileManager implements WolfieballFileManager {

    //JSON FILE READING AND WRITING CONSTANTS
    
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";
    String JSON_PLAYER_FIRST = "FIRST_NAME";
    String JSON_PLAYER_LAST ="LAST_NAME";
    String JSON_PLAYER_TEAM ="TEAM";
    String JSON_PLAYER_BIRTH="YEAR_OF_BIRTH";
    String JSON_PLAYER_NATION="NATION_OF_BIRTH";
    String JSON_PLAYER_NOTES="NOTES";
    String JSON_HITTER_QP="QP";
    String JSON_HITTER_AB="AB";
    String JSON_HITTER_R="R";
    String JSON_HITTER_H="H";
    String JSON_HITTER_HR="HR";
    String JSON_HITTER_RBI="RBI";
    String JSON_HITTER_SB="SB";
    String JSON_PITCHER_IP="IP";
    String JSON_PITCHER_ER="ER";
    String JSON_PITCHER_W="W";
    String JSON_PITCHER_SV="SV";
    String JSON_PITCHER_H="H";
    String JSON_PITCHER_BB="BB";
    String JSON_PITCHER_K="K";
    
    String JSON_EXT = ".json";
    String SLASH = "/";
    
    @Override
    public void saveDraft(Draft draft,String nameDraft) throws IOException {
        
        System.out.println(nameDraft);
        String draftListing = "";
        if(!nameDraft.equals(""))
            draftListing+=nameDraft;
        else{
            draftListing+="Draft1";
        }
        String jsonFilePath = WDK_StartupConstants.PATH_DRAFT +SLASH+draftListing+ JSON_EXT;
    
        //INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        //MAKE A JSON ARRAY FOR THE PAGES ARRAY
        JsonArray teams = makeTeamsJsonArray(draft);
        JsonArray orders = makeDraftOrders(draft);
        JsonObject draftObject = Json.createObjectBuilder()
                                .add("TEAMS", teams)
                                .add("DRAFT_ORDER",orders)
                                .build();
        
        jsonWriter.writeObject(draftObject);
        
    }

    @Override
    public void loadDraft(Draft draft, String draftPath) throws IOException {
        JsonObject json = loadJSONFile(draftPath);
        
        draft.setTeams(FXCollections.observableArrayList());
        HashMap<String,Player> playerMap = draft.getPlayerHashMap();
        
        //team
        JsonArray jsonTeamsArray = json.getJsonArray("TEAMS");
        for(int i=0;i<jsonTeamsArray.size();i++){
           JsonObject teamObject = jsonTeamsArray.getJsonObject(i);
           Team t = new Team();
           t.setName(teamObject.getString("NAME"));
           t.setOwner(teamObject.getString("OWNER"));
           
           JsonArray jsonMemberArray = teamObject.getJsonArray("MEMBERS");
           for(int j=0;j<jsonMemberArray.size();j++){
               
               JsonObject mObject = jsonMemberArray.getJsonObject(j);
               String lastName = mObject.getString("LAST");
               Player p= playerMap.get(lastName);
               TeamMember member = new TeamMember(mObject.getString("FIRST"),lastName,mObject.getString("POSITION"),
                                                mObject.getString("CONTRACT"),t.getName(),mObject.getInt("SALARY"),p);
               
               
               t.getTeamMember().add(member);
               
               setnumLeftPlayerPosDow(member.getPosition(),t);
           }
           
           JsonArray jsonTaxiArray = teamObject.getJsonArray("TAXI");
           for(int j=0;j<jsonTaxiArray.size();j++){
               
               JsonObject mObject = jsonTaxiArray.getJsonObject(j);
               String lastName = mObject.getString("LAST");
               Player p = playerMap.get(lastName);
               TeamMember member = new TeamMember(mObject.getString("FIRST"),lastName,t.getName(),p);
          
               t.getTaxiMember().add(member);
               
           }
           draft.getTeams().add(t);
            
        }
        
        ObservableList<TeamMember> orders = FXCollections.observableArrayList();
        
        JsonArray jsonOrderArray = json.getJsonArray("DRAFT_ORDER");
        for(int i=0;i<jsonOrderArray.size();i++){
            JsonObject orderObject = jsonOrderArray.getJsonObject(i);
            TeamMember t = new TeamMember();
            t.setFirst(orderObject.getString("FIRST"));
            t.setLast(orderObject.getString("LAST"));
            t.setFantasyTeam(orderObject.getString("FANTASY"));
            t.setContract(orderObject.getString("CONTRACT"));
            t.setSalary(orderObject.getInt("SALARY"));
            
            orders.add(t);
        }
        
        draft.setDrafted(orders);
    }
      private void setnumLeftPlayerPosDow(String pos, Team team) {

        if (pos.equals("C")) {
            team.setNumPosDown(0);
        } else if (pos.equals("1B")) {
            team.setNumPosDown(1);
        } else if (pos.equals("CI")) {
            team.setNumPosDown(2);
        } else if (pos.equals("3B")) {
            team.setNumPosDown(3);
        } else if (pos.equals("2B")) {
            team.setNumPosDown(4);
        } else if (pos.equals("MI")) {
            team.setNumPosDown(5);
        } else if (pos.equals("SS")) {
            team.setNumPosDown(6);
        } else if (pos.equals("U")) {
            team.setNumPosDown(7);
        } else if (pos.equals("OF")) {
            team.setNumPosDown(8);
        } else if (pos.equals("P")) {
            team.setNumPosDown(9);
        }

    }

    @Override
    public void savePlayer(Player[] player) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Player> loadPlayers(String playerPath,String who) throws IOException {
    
        
        ObservableList<Player> temp = FXCollections.observableArrayList();
        if(who.equals(JSON_HITTERS))
            temp= loadArrayFromJSONFileHiiter(playerPath, who);
        else
            temp= loadArrayFromJSONFilePitcher(playerPath, who);
        
        return temp;
        
    }

        // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ObservableList<Player> loadArrayFromJSONFileHiiter(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ObservableList<Player> items = FXCollections.observableArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (int i=0;i<jsonArray.size();i++) {
            
            Player temp = buildHitterJsonObject(jsonArray.getJsonObject(i));
            items.add(temp);
        }
        return items;
    }
  
    
            // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ObservableList<Player> loadArrayFromJSONFilePitcher(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ObservableList<Player> items = FXCollections.observableArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (int i=0;i<jsonArray.size();i++) {
            
            Player temp = buildPitcherJsonObject(jsonArray.getJsonObject(i));
            items.add(temp);
        }
        return items;
    }
    public Player buildHitterJsonObject(JsonObject json) {
        Player player = new Player( json.getString(JSON_PLAYER_FIRST),
                                    json.getString(JSON_PLAYER_LAST),
                                    json.getString(JSON_PLAYER_TEAM),
                                    json.getString(JSON_PLAYER_NOTES),
                                    json.getString(JSON_PLAYER_NATION),
                                    json.getString(JSON_HITTER_QP),
                                    Integer.parseInt(json.getString(JSON_PLAYER_BIRTH)),
                                    Integer.parseInt(json.getString(JSON_HITTER_AB)),
                                    Integer.parseInt(json.getString(JSON_HITTER_RBI)),
                                    Integer.parseInt(json.getString(JSON_HITTER_HR)),
                                    Integer.parseInt(json.getString(JSON_HITTER_H)),
                                    Integer.parseInt(json.getString(JSON_HITTER_R)),
                                    -1.0,
                                    Integer.parseInt(json.getString(JSON_HITTER_SB)),
                                    true
                                    
                                    );
        return player;
    }
    public Player buildPitcherJsonObject(JsonObject json) {
        Player player = new Player( json.getString(JSON_PLAYER_FIRST),
                                    json.getString(JSON_PLAYER_LAST),
                                    json.getString(JSON_PLAYER_TEAM),
                                    json.getString(JSON_PLAYER_NOTES),
                                    json.getString(JSON_PLAYER_NATION),
                                    "P",
                                    Integer.parseInt(json.getString(JSON_PLAYER_BIRTH)),
                                    Integer.parseInt(json.getString(JSON_PITCHER_ER)),
                                    Integer.parseInt(json.getString(JSON_PITCHER_K)),
                                    Integer.parseInt(json.getString(JSON_PITCHER_SV)),
                                    Integer.parseInt(json.getString(JSON_PITCHER_H)),
                                    Integer.parseInt(json.getString(JSON_PITCHER_W)),
                                    Double.parseDouble(json.getString(JSON_PITCHER_IP)),
                                    Integer.parseInt(json.getString(JSON_PITCHER_BB)),
                                    false
                                    
                                    );
        return player;
    }
    
        // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    private JsonObject makeTaxiinfo(TeamMember member){
        JsonObject jso = Json.createObjectBuilder().add("FIRST", member.getFirst())
                                                   .add("LAST", member.getLast())
                                                   .build();
        return jso;
    }
    
    private JsonObject makePlayerinfo(TeamMember member){
        
        JsonObject jso = Json.createObjectBuilder().add("FIRST", member.getFirst())
                                                   .add("LAST", member.getLast())
                                                   .add("POSITION",member.getPosition())
                                                   .add("CONTRACT", member.getContract())
                                                   .add("SALARY", member.getSalary())
                                                   .build();
        
        return jso;
        
    }
    private JsonArray makeTaxiJsonArray(ObservableList<TeamMember> taxies){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(TeamMember t : taxies){
            jsb.add(makeTaxiinfo(t));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    private JsonArray makeMemberJsonArray(ObservableList<TeamMember> members){
        JsonArrayBuilder jsb= Json.createArrayBuilder();
        for(TeamMember t : members){
            jsb.add(makePlayerinfo(t));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private JsonObject makeTeamJsonObject(Team team){
        
        JsonArray members = makeMemberJsonArray(team.getTeamMember());
        JsonArray taxies = makeTaxiJsonArray(team.getTaxiMember());
        JsonObject jso = Json.createObjectBuilder().add("NAME",team.getName())
                                                  .add("OWNER",team.getOwner())
                                                  .add("MEMBERS",members)
                                                  .add("TAXI", taxies)
                                                  .build();
        
        return jso;
        
    }
    
    private JsonArray makeTeamsJsonArray(Draft draft){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(Team t : draft.getTeams())
            jsb.add(makeTeamJsonObject(t));
        
        JsonArray jA = jsb.build();
        return jA;
    }
    private JsonArray makeDraftOrders(Draft draft){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(TeamMember t : draft.getDrafted()){
            jsb.add(makeDraftOrder(t));
            
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    private JsonObject makeDraftOrder(TeamMember teamMember){
        JsonObject jso = Json.createObjectBuilder().add("FIRST",teamMember.getFirst())
                                                   .add("LAST",teamMember.getLast())
                                                   .add("FANTASY", teamMember.getFantasyTean())
                                                   .add("CONTRACT", teamMember.getContract())
                                                   .add("SALARY", teamMember.getSalary())
                                                   .build();
        
        return jso;
    }
}
