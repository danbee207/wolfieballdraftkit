/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.file;

import java.io.IOException;
import javafx.collections.ObservableList;
import wdf.data.Draft;
import wdf.data.Player;

/**
 * This interface provides an abstraction of what a file manager should do. Note
 * that file managers know how to read and write courses, instructors, and subjects,
 * but now how to export sites.
 * @author DnB
 */
public interface WolfieballFileManager {

    
    public void                      saveDraft(Draft draft,String nameDraft) throws IOException;
    public void                      loadDraft(Draft draft, String draftPath) throws IOException;
    public void                      savePlayer(Player[] player) throws IOException;
    public ObservableList<Player>         loadPlayers(String playerPath,String who) throws IOException;
}
