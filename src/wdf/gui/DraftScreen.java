/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import wdf.WDK_PropertyType;
import wdf.controller.DraftEditController;
import wdf.data.Team;
import wdf.data.TeamMember;

/**
 *
 * @author DnB
 */
public class DraftScreen extends TableScreen{

    DraftEditController draftEditController;
    
    TableView<TeamMember> draftTable;
    TableColumn itemPick;
    TableColumn itemFirst;
    TableColumn itemLast;
    TableColumn itemTeam;
    TableColumn itemContract;
    TableColumn itemSalary;
    
    Button automatedBtn;
    Button automatedPersonBtn;
    Button automatedPauseBtn;
    
    
    
    
    
    public DraftScreen(WDK_GUI gui) {
        super(gui);
        
        draftEditController = gui.getDraftController();
       
       
    }
 
    
    
  
    @Override
     public void initStyle(){
        super.initStyle();
     
        draftTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }
    
    @Override
    public void initLayout(){
 
        super.initLayout();
        
    }

    @Override
    protected void addtoToppart() {
        
        
        headingLabel.setText(props.getProperty(WDK_PropertyType.DRAF_HEADING_LABEL.toString())); 
        
        automatedPersonBtn = initHBoxButton(btnBox, WDK_PropertyType.AUTOMATED_PERSON_BTN, WDK_PropertyType.AUTOMATED_PERSON_BTN_TOOLTIP, "");
        automatedBtn       = initHBoxButton(btnBox, WDK_PropertyType.AUTOMATED_BTN, WDK_PropertyType.LOGO, "");
        automatedPauseBtn  = initHBoxButton(btnBox, WDK_PropertyType.AUTOMATED_PAUSE_BTN, WDK_PropertyType.AUTOMATED_PAUSE_BTN_TOOLTIP, "");
        
        toppart.getChildren().add(btnBox);

    }

    @Override
    protected void addtoCenterpart() {
        
        
        draftTable = new TableView();
        tableBox.getChildren().add(draftTable);
        
        initTableColumn();
        linkColumntoData();
        
        centerpart.getChildren().add(tableBox);
        
    }

    @Override
    protected void initEventHandler() {
        
        automatedBtn.setOnAction(e->{
            draftEditController.handleAutomatingPlayers(this);
        });
        automatedPauseBtn.setOnAction(e->{
            draftEditController.handleAutomatingPause(this);
        });
        automatedPersonBtn.setOnAction(e->{
            draftEditController.handleAutomatingPerson(this);
        });
        
    }

    private void initTableColumn() {
        
        itemPick = new TableColumn(props.getProperty(WDK_PropertyType.COL_PICK.toString()));
        itemFirst = new TableColumn(props.getProperty(WDK_PropertyType.COL_FIRST.toString()));
        itemLast = new TableColumn(props.getProperty(WDK_PropertyType.COL_LAST.toString()));
        itemTeam = new TableColumn(props.getProperty(WDK_PropertyType.COL_FANTASY_TEAM.toString()));
        itemContract = new TableColumn(props.getProperty(WDK_PropertyType.COL_CONTRACT.toString()));
        itemSalary = new TableColumn(props.getProperty(WDK_PropertyType.COL_SALARY.toString()));
        
    
    }

    private void linkColumntoData() {
        
        itemPick.setCellValueFactory(new Callback<CellDataFeatures<TeamMember, TeamMember>, ObservableValue<TeamMember>>() {@Override
            public ObservableValue<TeamMember> call(CellDataFeatures<TeamMember, TeamMember> param) {
                return new ReadOnlyObjectWrapper(param.getValue());            }
        });

        itemPick.setCellFactory(new Callback<TableColumn<TeamMember, TeamMember>, TableCell<TeamMember, TeamMember>>() {
            @Override public TableCell<TeamMember, TeamMember> call(TableColumn<TeamMember, TeamMember> param) {
                return new TableCell<TeamMember, TeamMember>() {
                    @Override protected void updateItem(TeamMember item, boolean empty) {
                        super.updateItem(item, empty);

                        if (this.getTableRow() != null && item != null) {
                            setText(this.getTableRow().getIndex()+"");
                        } else {
                            setText("");
                        }
                    }
                };
            }
        });
        itemPick.setSortable(false);
        
        itemFirst.setCellValueFactory(new PropertyValueFactory<TeamMember,String>("first"));
        itemLast.setCellValueFactory(new PropertyValueFactory<TeamMember,String>("last"));
        itemTeam.setCellValueFactory(new PropertyValueFactory<TeamMember,String>("fantasyTeam"));
        itemContract.setCellValueFactory(new PropertyValueFactory<TeamMember,String>("contract"));
        itemSalary.setCellValueFactory(new PropertyValueFactory<TeamMember,Integer>("salary"));
    
    
        draftTable.getColumns().addAll(itemPick,itemFirst,itemLast,itemTeam,itemContract,itemSalary);
        draftTable.setItems(getDdataManager().getDraft().getDrafted());
    }

    public TableView<TeamMember> getDraftTable() {
        return draftTable;
    }
    
    
    
    
    
    
    
}
