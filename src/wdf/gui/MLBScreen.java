/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import properties_manager.PropertiesManager;
import wdf.WDK_PropertyType;
import static wdf.WDK_PropertyType.ADD_TEAM_TOOLTIP;
import static wdf.WDK_PropertyType.SELECT_MLB_TEAM_LABEL;
import wdf.data.LeagueTeam;
import wdf.data.Player;
import static wdf.gui.TeamsScreen.CLASS_COMBOBOX;

/**
 *
 * @author DnB
 */



public class MLBScreen extends TableScreen{

    
    ObservableList<Player> allplayers;
    ObservableList<Player> teamPlayers;
    
    Label selectLabel;
    ComboBox mlbTeamComboBox; 
    
    //to resize combobox
    Button addBtn;
    
    TableView<Player> mlbTable;
    TableColumn itemFirst;
    TableColumn itemLast;
    TableColumn itemPositions;
            
    
    
    public MLBScreen(WDK_GUI gui) {
        super(gui);
        
        allplayers = ddataManager.getDraft().getAllPlayers();
        teamPlayers = FXCollections.observableArrayList();
    }

    @Override
    public void initLayout() {
        super.initLayout(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initStyle() {
        super.initStyle(); //To change body of generated methods, choose Tools | Templates.
        
        mlbTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }


    @Override
    protected void addtoToppart() {
        
        headingLabel.setText(props.getProperty(WDK_PropertyType.MLB_HEADING_LABEL.toString()));
   
        selectLabel = initHBoxLabel(searchBox,SELECT_MLB_TEAM_LABEL,CLASS_LABEL);
        
        addBtn = initButton(WDK_PropertyType.ADD_ICON, ADD_TEAM_TOOLTIP);
       
        mlbTeamComboBox = new ComboBox();
        mlbTeamComboBox.setPrefHeight(imageBtnSize-5.0);
        mlbTeamComboBox.setPrefWidth(imageBtnSize *17);
        mlbTeamComboBox.getStyleClass().add(CLASS_COMBOBOX);
        
        mlbTeamComboBox.getSelectionModel().selectFirst();
        ObservableList<String> mlbTeams = FXCollections.observableArrayList();
        for(LeagueTeam l : LeagueTeam.values()){
            mlbTeams.add(l.toString());
        }
        mlbTeamComboBox.setItems(mlbTeams);
        mlbTeamComboBox.getSelectionModel().select(0);
        
        searchBox.getChildren().add(mlbTeamComboBox);
        
        toppart.getChildren().add(searchBox);
        
    }

    @Override
    protected void addtoCenterpart() {
        //SET TABLE
        mlbTable = new TableView();
        tableBox.getChildren().add(mlbTable);
        
        initTableColumn();
        linkColumntoData();
        
        centerpart.getChildren().add(tableBox);
        

        
    }

    @Override
    protected void initEventHandler() {
        
        mlbTeamComboBox.valueProperty().addListener((ov,oldValue,newValue)->{
            if(newValue!=null){
                teamPlayers.clear();
                for(Player p : allplayers){
                    if(p.getTeam().equals(newValue)){
                        teamPlayers.add(p);
                    }
                }
                Collections.sort(teamPlayers);
            }
        
        });
    }

    private void initTableColumn() {
        
        itemFirst = new TableColumn(props.getProperty(WDK_PropertyType.COL_FIRST.toString()));
        itemLast  = new TableColumn(props.getProperty(WDK_PropertyType.COL_LAST.toString()));
        itemPositions = new TableColumn(props.getProperty(WDK_PropertyType.COL_POS.toString()));
        
    }

    private void linkColumntoData() {

        itemFirst.setCellValueFactory(new PropertyValueFactory<Player,String>("first"));
        itemLast.setCellValueFactory(new PropertyValueFactory<Player,String>("last"));
        itemPositions.setCellValueFactory(new PropertyValueFactory<Player,String>("qp"));
        
        mlbTable.getColumns().addAll(itemFirst,itemLast,itemPositions);
        
        for(Player p : allplayers){
            if(p.getTeam().equals(LeagueTeam.ATL.toString())){
                teamPlayers.add(p);
            }
            
        }
        
        Collections.sort(teamPlayers);
        
        mlbTable.setItems(teamPlayers);
    }
    
    
    
}
