/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdf.WDK_StartupConstants.PATH_CSS;

/**
 * This class serves to present custom text messages to the user when
 * events occur. Note that it always provides the same controls, a label
 * with a message, and a single ok button. The scheduled release of 
 * Java 8 version 40 in March 2015 will make this class irrelevant 
 * because the Alert class will do what this does and much more.
 * 
 * @author DnB
 */
public class MessageDialog extends Stage{
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "Dialog_style.css";
    static final String CLASS_PANE= "paneStyle";
    static final String CLASS_BUTTON = "button";
    static final String CLASS_LABEL = "heading_label";
    
     
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Button closeButton;
    
    
    /**
     * Initializes this dialog so that it can be used repeatedly
     * for all kinds of messages.
     
     * @param owner The owner stage of this modal dialoge.
     * @param closeButtonText Text to appear on the close button.
     */
    public MessageDialog(Stage owner, String closeButtonText){
       //MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        //LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();
        
        //CLOSE BUTION
        closeButton = new Button(closeButtonText);
        closeButton.setOnAction(e->{MessageDialog.this.close();});
        
        
        //WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().addAll(messageLabel,closeButton);
        
        //MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);
        
        //AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
        
       
    }
    
    public void show(String message){
        
        
        messageLabel.setText(message);
        initStylesheet();
        this.showAndWait();
    }
    
    /**
     * ADD STYLE TO ELEMENT(SCENE, LABEL, BUTTON)
     */
    private void initStylesheet(){
        
        messageScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        
        messageLabel.getStyleClass().add(CLASS_LABEL);
        closeButton.getStyleClass().add(CLASS_BUTTON);
        
    }
}
