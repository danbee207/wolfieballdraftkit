/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import wdf.data.Draft;
import wdf.data.LeagueTeam;
import wdf.data.Player;
import wdf.data.TeamMember;

/**
 *
 * @author DnB
 */
public class PlayerAddDialog extends PlayerDialog {

    HBox fnameBox, lnameBox, teamBox, posBox;
    Label fnameLabel;
    Label lnameLabel;
    TextField fnameTextField;
    TextField lnameTextField;
    Label teamLabel;
    ComboBox teamComboBox;

    CheckBox[] cbs = new CheckBox[pos.length];
    ObservableList<String> positions;

    public static final String ADD_PLAYER_TITLE = "Add New Player";
    public static final String FIRST_PROMPT = "First Name: ";
    public static final String LAST_PROMPT = "Last Name : ";
    public static final String TEAM_PROMPT = "Pro Team  : ";
    final static String[] pos = new String[]{"C", "1B", "2B", "3B", "SS", "OF", "P"};

    public static final String CLASS_LABEL = "ask_label";

    public PlayerAddDialog(Stage initStage, MessageDialog initMessageDialog) {
        super(initStage, initMessageDialog);

        //NOW THE NAME
        fnameLabel = new Label(FIRST_PROMPT);
        fnameTextField = new TextField("");
        fnameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirst(newValue);

        });
        fnameLabel.getStyleClass().add(CLASS_LABEL);
        fnameBox = new HBox();
        fnameBox.getChildren().addAll(fnameLabel, fnameTextField);

        lnameLabel = new Label(LAST_PROMPT);
        lnameTextField = new TextField("");
        lnameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLast(newValue);

        });
        lnameLabel.getStyleClass().add(CLASS_LABEL);
        lnameBox = new HBox();
        lnameBox.getChildren().addAll(lnameLabel, lnameTextField);

        teamLabel = new Label(TEAM_PROMPT);
        ObservableList<String> leagueTeamList = FXCollections.observableArrayList();
        for (LeagueTeam league : LeagueTeam.values()) {
            leagueTeamList.add(league.toString());
        }
        teamComboBox = new ComboBox(leagueTeamList);
        teamComboBox.valueProperty().addListener((ov, oldValue, newValue) -> {

            player.setTeam(newValue.toString());

        });
        teamBox = new HBox();
        teamBox.getChildren().addAll(teamLabel, teamComboBox);

        positions = FXCollections.observableArrayList();

        posBox = new HBox();

        for (int i = 0; i < pos.length; i++) {
            cbs[i] = new CheckBox(pos[i]);
            posBox.getChildren().add(cbs[i]);
            CheckBox temp = cbs[i];
            temp.selectedProperty().addListener((ov, oldVal, newVal) -> {
                String poses = "";

                if (temp.getText().equals("P")) {
                    player.setHitter(!newVal);
                    
                    for(int j=0;j<pos.length-1;j++){
                        cbs[j].setDisable(newVal);
                        if(newVal)
                            cbs[j].setSelected(!newVal);
                    }
                }else{
                    
                        cbs[pos.length-1].setDisable(newVal);
                        if(newVal)
                            cbs[pos.length-1].setSelected(!newVal);
                    
                }

                if (newVal) {
                    positions.add(temp.getText());
                } else {
                    positions.remove(temp.getText());
                }
               
                if(positions.size()>1){
                    for(int j=0;j<positions.size()-1;j++){
                        poses +=positions.get(j)+"_";
                    }
                    poses += positions.get(positions.size()-1);
                }
                else if(positions.size()==1)
                    poses = positions.get(0);
                player.setQp(poses);
                
            });
        }

        outlineBox.getChildren().addAll(fnameBox, lnameBox, teamBox, posBox, btnBox);

        EventHandler completelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            boolean ch = true;
            if (fnameTextField.getText().isEmpty() || lnameTextField.getText().isEmpty() || player.getQp().isEmpty()) {
                ch = false;
            }

            if (ch) {
                PlayerAddDialog.this.selection = sourceButton.getText();
                PlayerAddDialog.this.hide();
            } else {
                messageDialog.show("You should input all information");
            }
        };

        completeBtn.setOnAction(completelHandler);

        posBox.setSpacing(3);

        VBox temp = new VBox();
        temp.getChildren().add(outlineBox);
        temp.setPadding(new Insets(20, 20, 20, 20));
        dialogScene = new Scene(temp);
        this.setScene(dialogScene);

    }

    @Override
    public Player showAddPlayerDailog() {

        setTitle(ADD_PLAYER_TITLE);

        player = new Player();

        teamComboBox.getSelectionModel().selectFirst();

        this.showAndWait();
        return player;

    }

    @Override
    public void showEditPlayerDialog(TeamMember player, Draft draft) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
