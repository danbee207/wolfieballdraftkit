/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdf.WDK_StartupConstants.PATH_CSS;
import wdf.data.Draft;
import wdf.data.Player;
import wdf.data.TeamMember;

/**
 *
 * @author DnB
 */
public abstract class PlayerDialog extends Stage {
    
    //THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;
    Draft draft;
    
    //AND THIS HAD DATA INFO TO HELP WITH WITH VERIFICATION 
    
    //WE NEED THIS FOR FEEDBACK
    MessageDialog messageDialog;
    
    //GUI CONTROLS FOR OUR DIALOG
    
    VBox outlineBox;
    Scene dialogScene;
    Label headingLabel;

    HBox btnBox;
    Button completeBtn;
    Button cancelBtn;
    
    //THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection="";
    
    //CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String PLAYER_HEADING = "Player Detatils";
    
    public static final String PRIMARY_STYLE_SHEET = PATH_CSS + "Dialog_style.css";
    public static final String CLASS_BOX = "paneStyle";
    public static final String CLASS_HADING = "heading_label";
    public static final String CLSSS_BUTTON = "button";
    
    public PlayerDialog(Stage initStage,MessageDialog initMessageDialog){
        
        messageDialog = initMessageDialog;
        
        initModality(Modality.WINDOW_MODAL);
        initOwner(initStage);
        
        //FIRST OUR CONTATINER
        outlineBox = new VBox();
        outlineBox.setPadding(new Insets(10,20,20,20));
        outlineBox.setSpacing(20);
        
        //PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND 
        //ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_HEADING);
        
        completeBtn = new Button(COMPLETE);
        cancelBtn = new Button(CANCEL);
        btnBox = new HBox();
        btnBox.setSpacing(20);
        btnBox.setAlignment(Pos.CENTER);
        btnBox.getChildren().addAll(completeBtn,cancelBtn);
        
        EventHandler cancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae)->{
            Button sourceButton = (Button)ae.getSource();
            PlayerDialog.this.selection=sourceButton.getText();
            PlayerDialog.this.hide();
        };
        
        //completeBtn.setOnAction(completeCancelHandler);
        cancelBtn.setOnAction(cancelHandler);
        
        outlineBox.getChildren().add(headingLabel);
        
        outlineBox.getStylesheets().add(PRIMARY_STYLE_SHEET);
        outlineBox.getStyleClass().add(CLASS_BOX);
        headingLabel.getStyleClass().add(CLASS_HADING);
        completeBtn.getStyleClass().add(CLSSS_BUTTON);
        cancelBtn.getStyleClass().add(CLSSS_BUTTON);
        

    }

    public Player getPlayer() {
        return player;
    }

    public String getSelection() {
        return selection;
    }
    
    public abstract Player showAddPlayerDailog();
    public abstract void showEditPlayerDialog(TeamMember player,Draft draft);

    public boolean wasCompleteSelected(){
        return selection.equals(COMPLETE);
    }
    


}
