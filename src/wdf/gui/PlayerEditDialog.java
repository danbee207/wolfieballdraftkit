/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wdf.WDK_PropertyType.PlAYER_BLANK;
import static wdf.WDK_StartupConstants.PATH_FLAG_IMAGES;
import static wdf.WDK_StartupConstants.PATH_PLAYER_IMAGES;
import wdf.data.Draft;
import wdf.data.Player;
import wdf.data.Team;
import wdf.data.TeamMember;

/**
 *
 * @author DnB
 */
public class PlayerEditDialog extends PlayerDialog {

    //GUI CONTROLS FOR OUR DIALOG\
    HBox imageBox, teamBox, posBox, contractBox, salaryBox;
    VBox playerinfoBox;
    ImageView playerImgView;
    ImageView flagImgView;
    Label nameLabel;
    Label qpLabel;
    Label teamLabel;
    ComboBox teamComboBox;
    Label posLabel;
    ComboBox posComboBox;
    Label contractLabel;
    ComboBox contractComboBox;
    Label salaryLabel;
    TextField salaryLabelTextField;

    VBox playerinfo;

    TeamMember member;
    String initPosition;
    String teamName;
    HashMap<String, Team> teamMap;
    String[] positions;
    ObservableList<String> teamComboList;
    ObservableList<String> posComboList;

    public static final String TEAM_PROMPT = "Fantasy Team : ";
    public static final String POS_PROMPT = "Positions    : ";
    public static final String CONTRACT_PROMPT = "Contract     : ";
    public static final String SALARY_PROMPT = "Salary($)    : ";
    public static final String DEFAULT_STRING = "";
    public static final String EDITING_PLAYER_TITLE = "Edit Player";
    private Object PLAYER_BLANK;

    public static final String CLASS_LABEL = "ask_label";

    public PlayerEditDialog(Stage initStage, MessageDialog initMessageDialog) {

        super(initStage, initMessageDialog);

        member = new TeamMember();

        playerImgView = new ImageView();
        flagImgView = new ImageView();
        nameLabel = new Label();
        nameLabel.getStyleClass().add(CLASS_LABEL);
        qpLabel = new Label();
        qpLabel.getStyleClass().add(CLASS_LABEL);
        teamLabel = new Label(TEAM_PROMPT);
        teamLabel.getStyleClass().add(CLASS_LABEL);
        teamComboBox = new ComboBox();
        
        teamComboList = FXCollections.observableArrayList();
        teamComboBox.setItems(teamComboList);
        
        posComboList = FXCollections.observableArrayList();
        posComboBox = new ComboBox();
        posComboBox.setItems(posComboList);
        teamComboBox.valueProperty().addListener((ov, oldValue, newValue) -> {

            teamName = newValue.toString();

            if (newValue.equals("FREE AGENT") || draft.isTaxiPossible()) {
                posComboBox.setDisable(true);
                contractComboBox.setDisable(true);
                salaryLabelTextField.setDisable(true);

                salaryLabelTextField.textProperty().set("0");
                
                if(draft.isTaxiPossible()){
                    
                    posComboBox.valueProperty().set("H");
                    contractComboBox.valueProperty().setValue("X");


                    member.setContract("X");
                    member.setPostion("H");
                    member.setSalary(1);
                }
                else{
                    
                    posComboBox.valueProperty().set("H");
                    contractComboBox.valueProperty().setValue("");
                    
                    member.backToFreeAgent();
                    
                }
                

            } else {
                posComboBox.setDisable(false);
                contractComboBox.setDisable(false);
                salaryLabelTextField.setDisable(false);

                contractComboBox.valueProperty().setValue("S2");
                
                posComboList.clear();
                Team team = teamMap.get(teamName);

                //taxi make up impossible
                for (String pos : positions) {
                    if (team != null && getnumLeftPlayerPos(pos, team) > 0) {
                        posComboList.add(pos);
                    }
                }
                if(initPosition==null){
                    posComboBox.getSelectionModel().select(0);
                }else{
                    posComboList.add(initPosition);
                    posComboBox.getSelectionModel().select(initPosition);
                }
                
                
            }

            member.setFantasyTeam(teamName);

        });

        posLabel = new Label(POS_PROMPT);
        posLabel.getStyleClass().add(CLASS_LABEL);

        posComboBox.valueProperty().addListener((ov, oldValue, newValue) -> {
            if (newValue != null) {
               
                member.setPostion(newValue.toString());
            }

        });
        contractLabel = new Label(CONTRACT_PROMPT);
        contractLabel.getStyleClass().add(CLASS_LABEL);
        contractComboBox = new ComboBox();
        contractComboBox.getItems().addAll("S2", "S1");
        contractComboBox.valueProperty().addListener((ov, oldValue, newValue) -> {

            member.setContract(newValue.toString());
        });
        salaryLabel = new Label(SALARY_PROMPT);
        salaryLabel.getStyleClass().add(CLASS_LABEL);
        salaryLabelTextField = new TextField();
        salaryLabelTextField.textProperty().addListener((ov, oldValue, newValue) -> {
            try{
            member.setSalary(Integer.parseInt(newValue));
            }catch(NumberFormatException e){
                
            }

        });
        playerinfo = new VBox();
        playerinfo.getChildren().addAll(flagImgView, nameLabel, qpLabel);

        imageBox = new HBox();
        playerinfoBox = new VBox();
        playerinfoBox.setSpacing(15);
        playerinfoBox.getChildren().addAll(flagImgView, nameLabel, qpLabel);
        imageBox.getChildren().addAll(playerImgView, playerinfoBox);
        imageBox.setSpacing(10);
        teamBox = new HBox();
        teamBox.getChildren().addAll(teamLabel, teamComboBox);
        posBox = new HBox();
        posBox.getChildren().addAll(posLabel, posComboBox);
        contractBox = new HBox();
        contractBox.getChildren().addAll(contractLabel, contractComboBox);
        salaryBox = new HBox();
        salaryBox.getChildren().addAll(salaryLabel, salaryLabelTextField);

        outlineBox.getChildren().addAll(imageBox, teamBox, posBox, contractBox, salaryBox, btnBox);

        EventHandler completelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            boolean ch = false;
            String msg = "";
     
            Team team = teamMap.get(teamComboBox.getSelectionModel().getSelectedItem());
            //salary
            if (Integer.parseInt(salaryLabelTextField.getText()) > 0 && !draft.isTaxiPossible()) {

                ch = true;
                
                if(!member.getFantasyTean().equals("FREE AGENT")){
                    
                    if(team.getSalary()<team.numLeftPeople()+member.getSalary()){
                        ch=false;
                        msg = "You can't add left players. Fix this player's salary";
                    }
                    if(posComboBox.getSelectionModel().getSelectedItem()==null){
                        ch = false;
                        msg = "You should choose this player's position";
                    }
                        
                }
                
            } else if (draft.isTaxiPossible() ) {
                ch = true;
                if(team.getTaxinum()>8){
                    ch=false;
                    msg = "You cannot add new taxi member anymore";
                }
            } else if(team.getName().equals("FREE AGENT")){
                ch=true;
            
            }
            
            else {
                ch=false;
                msg = "You should input all correct information";
            }

            
            if (ch) {
                PlayerEditDialog.this.selection = sourceButton.getText();
                PlayerEditDialog.this.hide();
            } else {
                messageDialog.show(msg);
            }
        };

        completeBtn.setOnAction(completelHandler);

        VBox temp = new VBox();
        temp.setPadding(new Insets(20, 20, 20, 20));
        temp.getChildren().add(outlineBox);

        dialogScene = new Scene(temp);
        this.setScene(dialogScene);

    }

    @Override
    public Player showAddPlayerDailog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void showEditPlayerDialog(TeamMember player, Draft draft) {

        member.setPlayerInfo(player);
        this.draft = draft;
        setTitle(EDITING_PLAYER_TITLE);

        playerImgView.setImage(makeImage(player.getLast() + player.getFirst() + ".jpg", PATH_PLAYER_IMAGES));
        flagImgView.setImage(makeImage(player.getNation() + ".png", PATH_FLAG_IMAGES));
        nameLabel.setText(player.getFirst() + " " + player.getLast());
        qpLabel.setText(player.getQp());

        positions = player.getQp().split("_");

        teamName = player.getFantasyTean();
        teamMap = draft.getTeamsFromMappings();
        Team team = teamMap.get(player.getFantasyTean());

        
        
        //add left position in a team to posComboBox
        for (String pos : positions) {

            if (getnumLeftPlayerPos(pos, team) > 0) {
                posComboList.add(pos);
            }

        }
        
        //add the player's position to posComboBox
        
        
        if (!player.getPosition().equals("")) {
            initPosition = player.getPosition();
        }

        //add teams to teamComboBox
        for(Team t : draft.getTeams()){
            teamComboList.add(t.getName());
        }
        
        if(player.getFantasyTean().equals("FREE AGENT")){
            teamComboList.remove("FREE AGENT");
        }
        

        //set selection ,the teams which player is in, to teamComboBox
        if (player.getFantasyTean().equals("FREE AGENT")) {
            for(int i=1;i<draft.getTeams().size();i++){
                if(draft.getTeams().get(i).getNumLeft()>0){
                    teamComboBox.getSelectionModel().select(draft.getTeams().get(i).getName());
                    break;
                }
                    
            }
        } else {
            teamComboBox.getSelectionModel().select(player.getFantasyTean());
        }

        //set contract to contractComboBox
        if (player.getContract().equals("")) {

        } else {
            contractComboBox.getSelectionModel().select(player.getContract());
        }

        //set a player's salary to salarytxtField
        salaryLabelTextField.textProperty().setValue(Integer.toString(player.getSalary()));

        this.showAndWait();

    }

    private int getnumLeftPlayerPos(String pos, Team team) {
        
        if(pos.equals("C")){
            return team.getNumPos(0)+team.getNumPos(7);
        }else if(pos.equals("1B")){
            return team.getNumPos(1)+team.getNumPos(2)+team.getNumPos(7);
        }else if(pos.equals("3B")){
            return team.getNumPos(3)+team.getNumPos(2)+team.getNumPos(7);
        }else if(pos.equals("2B")){
            return team.getNumPos(4)+team.getNumPos(5)+team.getNumPos(7);
        }else if(pos.equals("SS")){
            return team.getNumPos(6)+team.getNumPos(5)+team.getNumPos(7);
        }else if(pos.equals("OF")){
            return team.getNumPos(8)+team.getNumPos(7);
        }else if(pos.equals("P")){
            return team.getNumPos(9);
        }
        

        return -1;
    }

    public String getTeamName() {
        return teamName;
    }

    public TeamMember getMember() {
        return member;
    }

    private Image makeImage(String filename, String whereimage) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + whereimage + filename;
        Image image = new Image(imagePath);

        if (image.isError()) {
            imagePath = "file:" + whereimage + props.getProperty(PlAYER_BLANK.toString());

            image = new Image(imagePath);
        }

        return image;
    }

}
