/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import wdf.WDK_PropertyType;
import static wdf.WDK_PropertyType.COL_BAWHIP;
import static wdf.WDK_PropertyType.COL_BIRTH;
import static wdf.WDK_PropertyType.COL_EVALUE;
import static wdf.WDK_PropertyType.COL_FIRST;
import static wdf.WDK_PropertyType.COL_HRSV;
import static wdf.WDK_PropertyType.COL_LAST;
import static wdf.WDK_PropertyType.COL_NOTES;
import static wdf.WDK_PropertyType.COL_POS;
import static wdf.WDK_PropertyType.COL_RBIK;
import static wdf.WDK_PropertyType.COL_RW;
import static wdf.WDK_PropertyType.COL_SBERA;
import static wdf.WDK_PropertyType.COL_TEAM;
import static wdf.WDK_PropertyType.RADIO_1B;
import static wdf.WDK_PropertyType.RADIO_2B;
import static wdf.WDK_PropertyType.RADIO_3B;
import static wdf.WDK_PropertyType.RADIO_ALL;
import static wdf.WDK_PropertyType.RADIO_C;
import static wdf.WDK_PropertyType.RADIO_CI;
import static wdf.WDK_PropertyType.RADIO_MI;
import static wdf.WDK_PropertyType.RADIO_OF;
import static wdf.WDK_PropertyType.RADIO_P;
import static wdf.WDK_PropertyType.RADIO_SS;
import static wdf.WDK_PropertyType.RADIO_U;
import wdf.controller.PlayerEditController;
import wdf.data.Player;
import wdf.data.TeamMember;

/**
 *
 * @author DnB
 */
public class PlayerScreen extends TableScreen {


    //PLAYER EDIT CONTROLLER
    PlayerEditController playerControler;

    //UI FOR TOP PART
    Button addBtn;
    Button minusBtn;
    Label searchLabel;
    TextField searchTxtField;

    //CHECK TEXT IN SEARCH FIELD
    String searchText = "";

    //UI FOR CENTER PART
    HBox radioBox;
    ToggleGroup radioGrup;
    RadioButton all, c, b1, c1, b3, b2, mi, ss, of, u, p;

    //TABLE
    TableView<TeamMember> itemsTable;

    TableColumn itemFirstColumn;
    TableColumn itemLastColumn;
    TableColumn itemTeamColumn;
    TableColumn itemPositionsColumn;
    TableColumn itemBirthColumn;
    TableColumn itemR_WColumn;
    TableColumn itemHR_SVColumn;
    TableColumn itemRBI_KColumn;
    TableColumn itemSB_ERAColumn;
    TableColumn itemBA_WHIPColumn;
    TableColumn itemEstimatedValueColumn;
    TableColumn itemNotesColumn;

    public PlayerScreen(WDK_GUI gui) {
        super(gui);
        playerControler = gui.getPlayerController();
    }



    @Override
    public void initLayout() {
        super.initLayout(); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public void initStyle() {
        super.initStyle(); //To change body of generated methods, choose Tools | Templates.

        radioBox.getStyleClass().add(CLASS_SCREEN);
        itemsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }



    @Override
    protected void initEventHandler() {

        radioGrup.selectedToggleProperty().addListener((ov, old_toggle, new_Toggle) -> {
            playerControler.handleRadioArrangements(this, new_Toggle, searchText);
        });

        registerTextFieldController(searchTxtField);

        addBtn.setOnAction(e -> {
            playerControler.handleNewPlayerRequest(this);
        });
        minusBtn.setOnAction(e -> {
            playerControler.handleRemovePlayerRequest(this, itemsTable.getSelectionModel().getSelectedItem());
        });
        itemsTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                playerControler.handleEditPlayerRequest(this, itemsTable.getSelectionModel().getSelectedItem());
            }
        });

    }

    private void registerTextFieldController(TextField textField) {

        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            searchText = newValue;
            playerControler.handleSearchRequest(this, pdataManager, newValue);

        });
    }

    @Override
    protected void addtoToppart() {

        headingLabel.setText(props.getProperty(WDK_PropertyType.PlAYER_HEADING_LABEL.toString()));
        
        //Set BtnBox
        addBtn = initHBoxButton(btnBox, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_PLAYER_TOOLTIP, "");
        minusBtn = initHBoxButton(btnBox, WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_PLAYER_TOOLTIP, "");

        addBtn.setPadding(new Insets(0, 0, 0, 0));
        minusBtn.setPadding(new Insets(0, 0, 0, 0));

        //Set searchBox
        searchLabel = initHBoxLabel(searchBox, WDK_PropertyType.PlAYER_SEARCH_LABEL, CLASS_LABEL);
        searchTxtField = initHBoxTextField(searchBox, 50, "", CLASS_TEXTFIELD,true);

        searchTxtField.setPrefHeight(imageBtnSize - 5.0);
       
        
        toppart.getChildren().add(editBox);

    }

    @Override
    protected void addtoCenterpart() {

        //RADIO BUTTON PART 
        radioBox = new HBox();
        radioGrup = new ToggleGroup();
        radioBox.setPadding(new Insets(10, 10, 10, 10));
        radioBox.setSpacing(30);
        radioBox.setAlignment(Pos.CENTER);

        initRadioBtns();
        all.setSelected(true);
        
        //TABLE VIEW PART
        initTable();
        linkColumntoData();
        
        centerpart.getChildren().addAll(radioBox, tableBox);
        
        

    }

    private void initTable() {

        itemsTable = new TableView();

        tableBox.getChildren().add(itemsTable);

        itemFirstColumn = new TableColumn(props.getProperty(COL_FIRST.toString()));
        itemLastColumn = new TableColumn(props.getProperty(COL_LAST.toString()));
        itemTeamColumn = new TableColumn(props.getProperty(COL_TEAM.toString()));
        itemPositionsColumn = new TableColumn(props.getProperty(COL_POS.toString()));
        itemBirthColumn = new TableColumn(props.getProperty(COL_BIRTH.toString()));
        itemR_WColumn = new TableColumn(props.getProperty(COL_RW.toString()));
        itemHR_SVColumn = new TableColumn(props.getProperty(COL_HRSV.toString()));
        itemRBI_KColumn = new TableColumn(props.getProperty(COL_RBIK.toString()));
        itemSB_ERAColumn = new TableColumn(props.getProperty(COL_SBERA.toString()));
        itemBA_WHIPColumn = new TableColumn(props.getProperty(COL_BAWHIP.toString()));
        itemEstimatedValueColumn = new TableColumn(props.getProperty(COL_EVALUE.toString()));
        itemNotesColumn = new TableColumn(props.getProperty(COL_NOTES.toString()));
    }

    private void linkColumntoData() {
        itemFirstColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("first"));
        itemLastColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("last"));
        itemTeamColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        itemPositionsColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("qp"));
        itemBirthColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("birth"));
        itemR_WColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("w_r"));
        itemHR_SVColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("hr_sv"));
        itemRBI_KColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("k_rbi"));

        itemBA_WHIPColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("ba_whipString"));
        itemEstimatedValueColumn.setCellValueFactory(new PropertyValueFactory<Player, Double>("estimatedValue"));
        itemNotesColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("notes"));

        itemSB_ERAColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("sb_era"));

        itemsTable.getColumns().addAll(itemFirstColumn, itemLastColumn, itemTeamColumn,
                itemPositionsColumn, itemBirthColumn, itemR_WColumn,
                itemHR_SVColumn, itemRBI_KColumn, itemSB_ERAColumn,
                itemBA_WHIPColumn, itemEstimatedValueColumn, itemNotesColumn);
        itemsTable.setItems(draft.getFreeagent().getTeamMember());

        itemEstimatedValueColumn.setCellFactory(col -> new TableCell<Player, Double>() {

            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (empty) {
                    setText(null);
                } else {
                   setText(String.format("%.3f", item));
                }
            }

        });

        noteColumeEditing();

    }
    
        private void noteColumeEditing() {

        itemsTable.setEditable(true);
        itemNotesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        itemNotesColumn.setOnEditCommit(new EventHandler<CellEditEvent<Player, String>>() {

            @Override
            public void handle(CellEditEvent<Player, String> event) {

                event.getTableView().getItems().get(event.getTablePosition().getRow()).setNotes(event.getNewValue());

            }

        });

    }

    private void initRadioBtns() {

        all = buildRadioBtn(props.getProperty(RADIO_ALL.toString()), props.getProperty(RADIO_ALL.toString()), radioGrup);
        c = buildRadioBtn(props.getProperty(RADIO_C.toString()), props.getProperty(RADIO_C.toString()), radioGrup);
        b1 = buildRadioBtn(props.getProperty(RADIO_1B.toString()), props.getProperty(RADIO_1B.toString()), radioGrup);
        c1 = buildRadioBtn(props.getProperty(RADIO_CI.toString()), props.getProperty(RADIO_CI.toString()), radioGrup);
        b3 = buildRadioBtn(props.getProperty(RADIO_3B.toString()), props.getProperty(RADIO_3B.toString()), radioGrup);
        b2 = buildRadioBtn(props.getProperty(RADIO_2B.toString()), props.getProperty(RADIO_2B.toString()), radioGrup);
        mi = buildRadioBtn(props.getProperty(RADIO_MI.toString()), props.getProperty(RADIO_MI.toString()), radioGrup);
        ss = buildRadioBtn(props.getProperty(RADIO_SS.toString()), props.getProperty(RADIO_SS.toString()), radioGrup);
        of = buildRadioBtn(props.getProperty(RADIO_OF.toString()), props.getProperty(RADIO_OF.toString()), radioGrup);
        u = buildRadioBtn(props.getProperty(RADIO_U.toString()), props.getProperty(RADIO_U.toString()), radioGrup);
        p = buildRadioBtn(props.getProperty(RADIO_P.toString()), props.getProperty(RADIO_P.toString()), radioGrup);

        all.setSelected(true);

    }

    private RadioButton buildRadioBtn(String label, String data, ToggleGroup group) {

        RadioButton btn = new RadioButton(label);
        btn.setUserData(data);
        btn.setToggleGroup(group);
        radioBox.getChildren().add(btn);
        btn.getStyleClass().add(CLASS_RADIO);
        return btn;
    }

    //GETTER and SETTER
    public PlayerEditController getPlayerControler() {
        return playerControler;
    }

    public void setPlayerControler(PlayerEditController playerControler) {
        this.playerControler = playerControler;
    }

    public TableView<TeamMember> getItemsTable() {
        return itemsTable;
    }

    public TableColumn getItemR_WColumn() {
        return itemR_WColumn;
    }

    public TableColumn getItemHR_SVColumn() {
        return itemHR_SVColumn;
    }

    public TableColumn getItemRBI_KColumn() {
        return itemRBI_KColumn;
    }

    public TableColumn getItemSB_ERAColumn() {
        return itemSB_ERAColumn;
    }

    public TableColumn getItemBA_WHIPColumn() {
        return itemBA_WHIPColumn;
    }

    public TextField getSearchTxtField() {
        return searchTxtField;
    }



    public ToggleGroup getRadioGrup() {
        return radioGrup;
    }

}
