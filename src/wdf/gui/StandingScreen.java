/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import wdf.WDK_PropertyType;
import wdf.data.ComparableTotalPoints;
import wdf.data.Team;

/**
 *
 * @author DnB
 */
public class StandingScreen extends TableScreen {

    TableView<Team> standingTable; 
    TableColumn itemName;
    TableColumn itemNeeded;
    TableColumn itemLeft;
    TableColumn itemPp;
    TableColumn itemR;
    TableColumn itemHr;
    TableColumn itemRbi;
    TableColumn itemSb;
    TableColumn itemBa;
    TableColumn itemW;
    TableColumn itemSv;
    TableColumn itemK;
    TableColumn itemEra;
    TableColumn itemWhip;
    TableColumn itemTotal;
    
    ObservableList<Team> standingList;
    
    public StandingScreen(WDK_GUI gui) {
        super(gui);
    }


    @Override
    public void initLayout() {
        super.initLayout(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initStyle() {
        super.initStyle(); //To change body of generated methods, choose Tools | Templates.
        standingTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }


    @Override
    protected void addtoToppart() {
        headingLabel.setText(props.getProperty(WDK_PropertyType.FANTASYSTANDING_HEADING_LABEL.toString()));
    }

    @Override
    protected void addtoCenterpart() {
        
     
        setTotalPointsSet();
        initTable();
        linkColumntoData();
        
        centerpart.getChildren().add(tableBox);
    }
    
    

    @Override
    protected void initEventHandler() {
    }
    
    

    private void initTable() {
        
        standingTable = new TableView();
        
        tableBox.getChildren().add(standingTable);
        
        itemName    = new TableColumn(props.getProperty(WDK_PropertyType.COL_TEAM_NAME.toString()));
        itemNeeded  = new TableColumn(props.getProperty(WDK_PropertyType.COL_PLAYERS_NEEDED.toString()));
        itemLeft    = new TableColumn(props.getProperty(WDK_PropertyType.COL_LEFT_MONEY.toString()));
        itemPp      = new TableColumn(props.getProperty(WDK_PropertyType.COL_PER_PLAYER.toString()));
        itemR       = new TableColumn(props.getProperty(WDK_PropertyType.COL_R.toString()));
        itemHr      = new TableColumn(props.getProperty(WDK_PropertyType.COL_HR.toString()));
        itemRbi     = new TableColumn(props.getProperty(WDK_PropertyType.COL_RBI.toString()));
        itemSb      = new TableColumn(props.getProperty(WDK_PropertyType.COL_SB.toString()));
        itemBa      = new TableColumn(props.getProperty(WDK_PropertyType.COL_BA.toString()));
        itemW       = new TableColumn(props.getProperty(WDK_PropertyType.COL_W.toString()));
        itemSv      = new TableColumn(props.getProperty(WDK_PropertyType.COL_SV.toString()));
        itemK       = new TableColumn(props.getProperty(WDK_PropertyType.COL_K.toString()));
        itemEra     = new TableColumn(props.getProperty(WDK_PropertyType.COL_ERA.toString()));
        itemWhip    = new TableColumn(props.getProperty(WDK_PropertyType.COL_WHIP.toString()));
        itemTotal   = new TableColumn(props.getProperty(WDK_PropertyType.COL_TOTAL_POINTS.toString()));
    
        
    }

    private void linkColumntoData() {

        itemName.setCellValueFactory(new PropertyValueFactory<Team,String>("name"));
        itemNeeded.setCellValueFactory(new PropertyValueFactory<Team,Integer>("numLeft"));
        itemLeft.setCellValueFactory(new PropertyValueFactory<Team,Integer>("salary"));
        itemPp.setCellValueFactory(new PropertyValueFactory<Team,Integer>("perplay"));
        itemR.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalR"));
        itemHr.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalHR"));
        itemRbi.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalRBI"));
        itemSb.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalSB"));
        itemBa.setCellValueFactory(new PropertyValueFactory<Team,Double>("totalBA"));
        itemW.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalW"));
        itemSv.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalSV"));
        itemK.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalK"));
        itemEra.setCellValueFactory(new PropertyValueFactory<Team,Double>("totalERA"));
        itemWhip.setCellValueFactory(new PropertyValueFactory<Team,Double>("totalWHIP"));
        itemTotal.setCellValueFactory(new PropertyValueFactory<Team,Integer>("totalPoints"));
        standingTable.getColumns().addAll(itemName,itemNeeded,itemLeft,itemPp,
                                            itemR,itemHr,itemRbi,itemSb,itemBa,itemW,
                                            itemSv,itemK,itemEra,itemWhip,itemTotal);
        
        standingTable.setItems(standingList);
        
        System.out.println(standingList.size());
        
        
        itemEra.setCellFactory(col->new TableCell<Team,Double>(){

            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if(empty){
                    setText(null);
                }
                else{
                    setText(String.format("%.2f", item));
                }
            }
        }
        );
        
        itemWhip.setCellFactory(col->new TableCell<Team,Double>(){

            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if(empty)
                    setText(null);
                else{
                    setText(String.format("%.2f", item));
                }
            }
        
        
        });
        
        itemBa.setCellFactory(col->new TableCell<Team,Double>(){

            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if(empty)
                    setText(null);
                else{
                    setText(String.format("%.3f", item));
                }
            }
        
        });
    
    }

    
    public void setTotalPointsSet() {

        standingList = FXCollections.observableArrayList();
        for(int i=1;i<ddataManager.getDraft().getTeams().size();i++)
            standingList.add(ddataManager.getDraft().getTeams().get(i));
        
        if(ddataManager.getDraft().isTaxiPossible()){
            if(standingList.size()>1)
                Collections.sort(standingList, new ComparableTotalPoints());
            else if(standingList.size()==1)
                standingList.get(0).setTotalPoints(10);
        }
    }
    
    
    
    
    
}
