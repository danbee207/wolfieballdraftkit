/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import wdf.WDK_PropertyType;
import static wdf.WDK_StartupConstants.PATH_CSS;
import static wdf.WDK_StartupConstants.PATH_IMAGES;
import wdf.data.Draft;
import wdf.manager.DraftDataManager;
import wdf.manager.PlayerDataManager;

/**
 *
 * @author DnB
 */
public abstract class TableScreen extends BorderPane {

    Draft draft;

    //DRAFT DATA MANAGER
    DraftDataManager ddataManager;
    
    //PLAYER DATA MANAGER
    PlayerDataManager pdataManager;

    //UI FOR TOP PART
    VBox toppart;
    HBox editBox;
    HBox btnBox;
    HBox searchBox;
    Label headingLabel;

    //UI FOR CENTER PART
    
    VBox centerpart;
    VBox tableBox;

    //RESIZE FOR EDITBUTTON SIZE
    public Double imageBtnSize = 0.0;

    //Properties Manager
    PropertiesManager props = PropertiesManager.getPropertiesManager();

    public static final String PRIMARY_STYLE_SHEET = PATH_CSS + "Screen_style.css";
    public static final String CLASS_HEADING = "heading";
    public static final String CLASS_SCREEN = "screen";
    public static final String CLASS_LABEL = "search";
    public static final String CLASS_RADIO = "radio";
    public static final String CLASS_TEXTFIELD = "textfieldStyle";

    public TableScreen(WDK_GUI gui){
        
        pdataManager = gui.getPlayermanager();
        ddataManager = gui.getDataManager();
        
        draft = ddataManager.getDraft();
        
        
    }
    
    
    public void initStyle() {

        this.getStylesheets().add(PRIMARY_STYLE_SHEET);
        headingLabel.getStyleClass().add(CLASS_HEADING);
        toppart.getStyleClass().add(CLASS_SCREEN);
        tableBox.getStyleClass().add(CLASS_SCREEN);

    }

    public void initLayout() {

        
        
        initBoxes();
        
        headingLabel = initVBoxLabel(toppart, WDK_PropertyType.PlAYER_HEADING_LABEL, CLASS_HEADING);

        addtoToppart();
        addtoCenterpart();

        //set padding
        setPaddings();


        this.setTop(toppart);
        this.setCenter(centerpart);

        initStyle();
        initEventHandler();
        
    }

    protected void setPaddings() {
        this.setPadding(new Insets(0, 10, 0, 10));
        toppart.setPadding(new Insets(10, 10, 10, 10));
        centerpart.setSpacing(10.0);

        toppart.setSpacing(15.0);
        btnBox.setSpacing(10.0);
        editBox.setSpacing(40.0);
        editBox.setAlignment(Pos.CENTER);
        searchBox.setAlignment(Pos.CENTER);
        searchBox.setSpacing(10.0);

        tableBox.setPadding(new Insets(10, 10, 20, 10));

        BorderPane.setMargin(toppart, new Insets(5, 0, 10, 0));
    }

    protected void initBoxes() {

        toppart = new VBox();

        editBox = new HBox();

        btnBox = new HBox();
        searchBox = new HBox();

        editBox.getChildren().addAll(btnBox, searchBox);

        centerpart = new VBox();
        tableBox = new VBox();

    }

    protected abstract void addtoToppart();

    protected abstract void addtoCenterpart();

    protected abstract void initEventHandler();



    // INIT A LABEL AND PLACE IT IN A VBOX INIT ITS PROPER PLACE
    protected Label initVBoxLabel(VBox container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }

    // INIT A LABEL AND PLACE IT IN A VBOX INIT ITS PROPER PLACE
    protected Label initHBoxLabel(HBox container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }

    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    protected Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    //INIT A BUTTON AND SET IT'S STYLESHEET CLASS
    protected Button initButton(WDK_PropertyType icon, WDK_PropertyType tooltip) {

        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        imageBtnSize = buttonImage.getHeight();
        Button button = new Button();
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);

        return button;
    }

    //INIT A BUTON AND PLACE IT IN A VBOX INIT ITS PROPER PLACE
    protected Button initHBoxButton(HBox container, WDK_PropertyType icon, WDK_PropertyType tooltip, String styleClass) {
        Button button = initButton(icon, tooltip);
        container.getChildren().add(button);
        return button;
    }

    // INIT A TEXT FIELD AND PUT IT IN A GridPane
    protected TextField initHBoxTextField(HBox container, int size, String initText,String classStyle, boolean editable) {

        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setText(initText);
        tf.setEditable(editable);
        tf.getStyleClass().add(classStyle);
        container.getChildren().add(tf);
        return tf;

    }

    public DraftDataManager getDdataManager() {
        return ddataManager;
    }

    public void setDdataManager(DraftDataManager ddataManager) {
        this.ddataManager = ddataManager;
    }

    public PlayerDataManager getPdataManager() {
        return pdataManager;
    }
    
    
}
