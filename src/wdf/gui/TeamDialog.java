/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdf.WDK_StartupConstants.PATH_CSS;
import wdf.data.Draft;
import wdf.data.Team;

/**
 *
 * @author DnB
 */
public class TeamDialog extends Stage{
    
    //THIS IS THE OBJECT DATA BEHIND THIS UI
    Team team;
    
    
    
    //AND THIS HAD DATA INFO TO HELP WITH VERIFICATION
    
    //WE NEED THIS FOR FEEDBACK
    MessageDialog messageDialog;
    
    //GUI CONTROLS FOR OUR DIALOG
    
    VBox outlineBox;
    HBox nameBox;
    HBox ownerBox;
    HBox btnBox;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    TextField nameTxtField;
    Label ownerLabel;
    TextField ownerTxtField;
    Button completeBtn;
    Button cancelBtn;
    
    //THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection ="";
    
    //CONSTANTS FOR OUR UI
    public static final String COMPLETE="Complete";
    public static final String CANCEL = "Cancel";
    public static final String TEAM_HEADING = "Fantasy Team Details";
    public static final String NAME_LABEL ="Name  ";
    public static final String OWNER_LABEL = "Owner ";
    
    public static final String PRIMARY_STYLE_SHEET = PATH_CSS + "Dialog_style.css";
    public static final String CLASS_GRID = "paneStyle";
    public static final String CLASS_HEADING = "heading_label";
    public static final String CLASS_OTHERS_LABEL = "ask_label";
    public static final String CLASS_BUTTON = "button";
    
    public static final String ADD_TEAM_TITLE = "Add new Team";
    public static final String EDIT_TEAM_TITLe = "Edit Team";
    
    public TeamDialog(Stage initStage, MessageDialog initMessageDialog){
        
        messageDialog = initMessageDialog;
        
        initModality(Modality.WINDOW_MODAL);
        initOwner(initStage);
        
        //FIRST OUR CONTAINER
        HBox temp = new HBox();
        temp.setPadding(new Insets(20,20,20,20));
        
        outlineBox = new VBox();
        outlineBox.setPadding(new Insets(20,20,20,20));
        outlineBox.setSpacing(10);
        nameBox = new HBox();
        ownerBox = new HBox();
        btnBox = new HBox();
        
        //PUT THE HEDING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        //ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(TEAM_HEADING);
        
        nameLabel = new Label(NAME_LABEL);
        nameTxtField = new TextField();
        nameBox.getChildren().addAll(nameLabel,nameTxtField);
        nameBox.setSpacing(5);
        nameBox.setAlignment(Pos.CENTER);
        
        nameTxtField.textProperty().addListener((observable,oldValue,newValue)->{
            
                team.setName(newValue);
            
        });
        
        ownerLabel = new Label(OWNER_LABEL);
        ownerTxtField = new TextField();
        ownerBox.getChildren().addAll(ownerLabel,ownerTxtField);
        ownerBox.setSpacing(5);
        ownerBox.setAlignment(Pos.CENTER);
        
        ownerTxtField.textProperty().addListener((observable,oldValue,newValue)->{
            team.setOwner(newValue);
        });
        
        completeBtn = new Button(COMPLETE);
        cancelBtn = new Button(CANCEL);
        btnBox.getChildren().addAll(completeBtn,cancelBtn);
        btnBox.setSpacing(15);
        btnBox.setAlignment(Pos.CENTER);
        
        
        
        EventHandler completecancelHandler = (EventHandler<ActionEvent>)(ActionEvent ae)->{       
          boolean ch = true;
          if(nameTxtField.textProperty().getValue().isEmpty() || ownerTxtField.textProperty().getValue().isEmpty())
              ch=false;
          
          Button sourceButton = (Button)ae.getSource();
          if(sourceButton.getText().equals(CANCEL))
              ch=true;
          if(ch){
             
           TeamDialog.this.selection = sourceButton.getText();
           TeamDialog.this.hide();
          }else{
              messageDialog.show("YOU SHOUld Inpust all thing");
          }
          
        };
        
        completeBtn.setOnAction(completecancelHandler);
        cancelBtn.setOnAction(completecancelHandler);
        

        
        outlineBox.getStylesheets().add(PRIMARY_STYLE_SHEET);
        outlineBox.getStyleClass().add(CLASS_GRID);
        headingLabel.getStyleClass().add(CLASS_HEADING);
        nameLabel.getStyleClass().add(CLASS_OTHERS_LABEL);
        ownerLabel.getStyleClass().add(CLASS_OTHERS_LABEL);
        completeBtn.getStyleClass().add(CLASS_BUTTON);
        cancelBtn.getStyleClass().add(CLASS_BUTTON);
        temp.getChildren().add(outlineBox);
        
        outlineBox.getChildren().addAll(headingLabel,nameBox,ownerBox,btnBox);
        
        dialogScene = new Scene(temp);
        this.setScene(dialogScene);
        
        
    
    }
    
    public Team getTeam(){
        return team;
    }
    
    public String getSelection(){
        return selection;
    }
    
    public Team showAddteamDialog(){
        
       
        //SET THE DIALOG TITLE
        setTitle(ADD_TEAM_TITLE);
        
        //RESET THE TEAM ITEM OBJECT WITH DEFAULT VALUES
        team = new Team();
        
        //LOAD THE UI STUFF
        nameTxtField.setText(team.getName());
        ownerTxtField.setText(team.getOwner());
        
        //AND OPEN IT UP
        this.showAndWait();
        
        return team;
    }
    
    public void loadGUIData(){
        
        //LOAD THE UI STUFF
        nameTxtField.setText(team.getName());
        ownerTxtField.setText(team.getOwner());
    }
    public void showEditTeamDialog(Team teamToEdit){
        
    
        //SET THE DIALOG TITLE
        setTitle(CANCEL);
        
        //LOAD THE TEAM INTO OUR LOCA OBJECT
        team.setName(teamToEdit.getName());
        
        team.setOwner(teamToEdit.getOwner());
        
        //AND TEHN INTO OUR GUI
        loadGUIData();
        
        //AND OPEN IT UP
        this.showAndWait();
        
    }
    
    public boolean wasCompleteSelected(){
        return selection.equals(COMPLETE);
    }
}
