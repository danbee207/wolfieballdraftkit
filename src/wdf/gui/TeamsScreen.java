/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.gui;

import java.util.HashMap;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import wdf.WDK_PropertyType;
import static wdf.WDK_PropertyType.DRAFT_NAME_LABEL;
import static wdf.WDK_PropertyType.LINEUP_LABEL;
import static wdf.WDK_PropertyType.TAXI_LABEL;
import wdf.controller.PlayerEditController;
import wdf.controller.TeamEditController;
import wdf.data.Player;
import wdf.data.Team;
import wdf.data.TeamMember;
import wdf.manager.TeamDataManager;

/**
 *
 * @author DnB
 */
public class TeamsScreen extends TableScreen {

    //TEAM DATA MANAGER
    TeamDataManager tdataManager;

    //TEAM EDIT CONTROLLER
    TeamEditController teamEditController;

    PlayerEditController playerControler;
    //HashMAP
    HashMap<String, Team> teamMap;

    //UI FOR TOP PART
    //UI FOR DRAFT BOX
    HBox draftBox;
    Label draftLabel;
    TextField draftTextField;

    // UI FOR BTNBOX
    Button addBtn;
    Button minusBtn;
    Button editBtn;

    //UI FOR SEARCHBOX
    Label teamLabel;
    ComboBox teamComboBox;

    //UI FOR CENTERPART
    Label lineupLabel;
    Label taxiLabel;

    TableView<TeamMember> lineupTable;
    TableColumn itemPosition;
    TableColumn itemFirst;
    TableColumn itemLast;
    TableColumn itemProTeam;
    TableColumn itemPositions;
    TableColumn itemR_W;
    TableColumn itemHR_SV;
    TableColumn itemRBI_K;
    TableColumn itemSB_ERA;
    TableColumn itemBA_WHIP;
    TableColumn itemEstivatedValue;
    TableColumn itemContract;
    TableColumn itemSalary;

    TableView<TeamMember> taxiTable;
    TableColumn itemTaxiPosition;
    TableColumn itemTaxiFirst;
    TableColumn itemTaxiLast;
    TableColumn itemTaxiProTeam;
    TableColumn itemTaxiPositions;
    TableColumn itemTaxiR_W;
    TableColumn itemTaxiHR_SV;
    TableColumn itemTaxiRBI_K;
    TableColumn itemTaxiSB_ERA;
    TableColumn itemTaxiBA_WHIP;
    TableColumn itemTaxiEstivatedValue;
    TableColumn itemTaxiContract;
    TableColumn itemTaxiSalary;

    VBox taxitableBox;

    public final static String CLASS_COMBOBOX = "comboBoxStyle";

    public TeamsScreen(WDK_GUI gui) {

        super(gui);

        tdataManager = gui.getTeamManager();
        teamEditController = gui.getTeamController();

        playerControler = gui.getPlayerController();

    }

    @Override
    public void initStyle() {
        super.initStyle();
        taxitableBox.getStyleClass().add(CLASS_SCREEN);

        lineupTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        taxiTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }

    @Override
    public void initLayout() {
        super.initLayout();

    }

    @Override
    protected void addtoToppart() {

        headingLabel.setText(props.getProperty(WDK_PropertyType.FANTASYTEAMS_HEADING_LABEL.toString()));

        //Set DRAFT BOX
        draftBox = new HBox();
        draftLabel = initHBoxLabel(draftBox, DRAFT_NAME_LABEL, CLASS_LABEL);
        draftTextField = initHBoxTextField(draftBox, 60, "", CLASS_TEXTFIELD, true);
        draftTextField.setPrefHeight(imageBtnSize - 5.0);
        draftTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            ddataManager.getDraft().setSaveToName(newValue);
        });

        //SET BTNBOX
        addBtn = initHBoxButton(btnBox, WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_TEAM_TOOLTIP, "");
        minusBtn = initHBoxButton(btnBox, WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_TEAM_TOOLTIP, "");
        editBtn = initHBoxButton(btnBox, WDK_PropertyType.EDIT_ICON, WDK_PropertyType.EDIT_TEAM_TOOLTIP, "");

        //SET SEARCHBOX
        teamLabel = initHBoxLabel(searchBox, WDK_PropertyType.SELECT_TEAM_LABEL, CLASS_LABEL);
        teamComboBox = new ComboBox();
        teamComboBox.setPrefHeight(imageBtnSize - 5.0);
        teamComboBox.setPrefWidth(imageBtnSize * 17);
        teamComboBox.getStyleClass().add(CLASS_COMBOBOX);

        teamComboBox.getSelectionModel().selectFirst();

        for (int i = 1; i < draft.getTeams().size(); i++) {
            teamComboBox.getItems().add(draft.getTeams().get(i).getName());
        }

        searchBox.getChildren().add(teamComboBox);

        toppart.getChildren().addAll(draftBox, editBox);

    }

    @Override
    protected void addtoCenterpart() {
        //SET LIST TABLE
        lineupLabel = initVBoxLabel(tableBox, LINEUP_LABEL, CLASS_LABEL);
        lineupTable = new TableView();
        tableBox.getChildren().add(lineupTable);

        //SET TAXI TABLE
        taxiLabel = initVBoxLabel(taxitableBox, TAXI_LABEL, CLASS_LABEL);
        taxiTable = new TableView();
        taxitableBox.getChildren().add(taxiTable);

        initTablColumn();
        
        linkColumntoData();
        linkTaxiColumntoData();

        
        centerpart.getChildren().addAll(tableBox, taxitableBox);
    }

    @Override
    protected void initEventHandler() {

        teamComboBox.valueProperty().addListener((ov, oldValue, newValue) -> {
            teamMap = draft.getTeamsFromMappings();

            if (newValue != null) {
                if (teamMap.get(newValue.toString()).getTeamMember().size() > 0) {
                    lineupTable.setItems(teamMap.get(newValue.toString()).getTeamMember());
                }else{
                    lineupTable.setItems(null);
                }
                
                if(teamMap.get(newValue.toString()).getTaxiMember().size()>0)
                    taxiTable.setItems(teamMap.get(newValue.toString()).getTaxiMember());
                else
                    taxiTable.setItems(null);
            } else {
                lineupTable.setItems(null);
                taxiTable.setItems(null);
            }
        });

        addBtn.setOnAction(e -> {
            Team team = teamEditController.handleNewTeamRequest(this);
            if (team != null) {
                teamComboBox.getItems().add(team.getName());
                teamMap = draft.getTeamsFromMappings();
                addChangeListener(team);
            }

        });
        minusBtn.setOnAction(e -> {
            HashMap<String, Team> teamMap = getDdataManager().getDraft().getTeamsFromMappings();
            Team tCheck = (Team) teamMap.get(teamComboBox.valueProperty().getValue());

            if (tCheck != null) {
                Team team = teamEditController.handleRemoveTeamRequest(this, teamMap.get(teamComboBox.getSelectionModel().getSelectedItem()));
                if (team != null) {
                    teamComboBox.getItems().remove(team.getName());
                }
            }

        });
        editBtn.setOnAction(e -> {
            Team team = teamMap.get(teamComboBox.getSelectionModel().getSelectedItem());

            String teamName = team.getName();
            team = teamEditController.handleEditTeamRequest(this, teamMap.get(teamComboBox.getSelectionModel().getSelectedItem()));

            if (teamName.equals(team.getName())) {

            } else {

                teamComboBox.getItems().set(teamComboBox.getItems().indexOf(teamName), team.getName());
                teamComboBox.getSelectionModel().select(team.getName());

            }

        });

        lineupTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                playerControler.handleEditPlayerRequest(this, lineupTable.getSelectionModel().getSelectedItem());
            }

        });
        
        taxiTable.setOnMouseClicked(e->{
            if(e.getClickCount()==2){
                playerControler.handleEditTaxiPlayerRequest(this,taxiTable.getSelectionModel().getSelectedItem());
            }
        
        });

    }

    private void addChangeListener(Team tMembers) {

        tMembers.getTeamMember().addListener(new ListChangeListener<TeamMember>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends TeamMember> c) {
                System.out.println("Edit");
                HashMap<String, Team> teamMap = draft.getTeamsFromMappings();
                Team team = teamMap.get(teamComboBox.getSelectionModel().getSelectedItem());
                if (team != null && team.getTeamMember() != null) {
                    lineupTable.setItems(team.getTeamMember());
                }
                
                if(team!=null && team.getTaxiMember()!=null)
                    taxiTable.setItems(team.getTaxiMember());

            }
        });

    }

    @Override
    protected void initBoxes() {
        super.initBoxes(); //To change body of generated methods, choose Tools | Templates.
        taxitableBox = new VBox();
    }

    @Override
    protected void setPaddings() {
        super.setPaddings(); //To change body of generated methods, choose Tools | Templates.

        taxitableBox.setPadding(new Insets(10, 10, 20, 10));
        lineupLabel.setPadding(new Insets(0, 0, 10, 0));
        taxiLabel.setPadding(new Insets(0, 0, 10, 0));
    }

    private void initTablColumn() {

        itemPosition = new TableColumn(props.getProperty(WDK_PropertyType.COL_POSITION.toString()));
        itemFirst = new TableColumn(props.getProperty(WDK_PropertyType.COL_FIRST.toString()));
        itemLast = new TableColumn(props.getProperty(WDK_PropertyType.COL_LAST.toString()));
        itemProTeam = new TableColumn(props.getProperty(WDK_PropertyType.COL_TEAM.toString()));
        itemPositions = new TableColumn(props.getProperty(WDK_PropertyType.COL_POS.toString()));
        itemR_W = new TableColumn(props.getProperty(WDK_PropertyType.COL_RW.toString()));
        itemHR_SV = new TableColumn(props.getProperty(WDK_PropertyType.COL_HRSV.toString()));
        itemRBI_K = new TableColumn(props.getProperty(WDK_PropertyType.COL_RBIK.toString()));
        itemSB_ERA = new TableColumn(props.getProperty(WDK_PropertyType.COL_SBERA.toString()));
        itemBA_WHIP = new TableColumn(props.getProperty(WDK_PropertyType.COL_BAWHIP.toString()));
        itemEstivatedValue = new TableColumn(props.getProperty(WDK_PropertyType.COL_EVALUE.toString()));
        itemContract = new TableColumn(props.getProperty(WDK_PropertyType.COL_CONTRACT.toString()));
        itemSalary = new TableColumn(props.getProperty(WDK_PropertyType.COL_SALARY.toString()));
        
        itemTaxiPosition = new TableColumn(props.getProperty(WDK_PropertyType.COL_POSITION.toString()));
        itemTaxiFirst = new TableColumn(props.getProperty(WDK_PropertyType.COL_FIRST.toString()));
        itemTaxiLast = new TableColumn(props.getProperty(WDK_PropertyType.COL_LAST.toString()));
        itemTaxiProTeam = new TableColumn(props.getProperty(WDK_PropertyType.COL_TEAM.toString()));
        itemTaxiPositions = new TableColumn(props.getProperty(WDK_PropertyType.COL_POS.toString()));
        itemTaxiR_W = new TableColumn(props.getProperty(WDK_PropertyType.COL_RW.toString()));
        itemTaxiHR_SV = new TableColumn(props.getProperty(WDK_PropertyType.COL_HRSV.toString()));
        itemTaxiRBI_K = new TableColumn(props.getProperty(WDK_PropertyType.COL_RBIK.toString()));
        itemTaxiSB_ERA = new TableColumn(props.getProperty(WDK_PropertyType.COL_SBERA.toString()));
        itemTaxiBA_WHIP = new TableColumn(props.getProperty(WDK_PropertyType.COL_BAWHIP.toString()));
        itemTaxiEstivatedValue = new TableColumn(props.getProperty(WDK_PropertyType.COL_EVALUE.toString()));
        itemTaxiContract = new TableColumn(props.getProperty(WDK_PropertyType.COL_CONTRACT.toString()));
        itemTaxiSalary = new TableColumn(props.getProperty(WDK_PropertyType.COL_SALARY.toString()));

    }

    private void linkTaxiColumntoData() {

        itemTaxiPosition.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("position"));
        itemTaxiFirst.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("first"));
        itemTaxiLast.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("last"));
        itemTaxiProTeam.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("team"));
        itemTaxiPositions.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("qp"));
        itemTaxiR_W.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("w_r"));
        itemTaxiHR_SV.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("hr_sv"));
        itemTaxiRBI_K.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("k_rbi"));
        itemTaxiSB_ERA.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("sb_era"));
        itemTaxiBA_WHIP.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("ba_whipString"));
        itemTaxiEstivatedValue.setCellValueFactory(new PropertyValueFactory<TeamMember, Double>("estimatedValue"));
        itemTaxiContract.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("contract"));
        itemTaxiSalary.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("salary"));

        taxiTable.getColumns().addAll(itemTaxiPosition, itemTaxiFirst, itemTaxiLast, itemTaxiProTeam, itemTaxiPositions,
                itemTaxiR_W, itemTaxiHR_SV, itemTaxiRBI_K, itemTaxiSB_ERA, itemTaxiBA_WHIP, itemTaxiEstivatedValue,
                itemTaxiContract, itemTaxiSalary);

        

        itemTaxiEstivatedValue.setCellFactory(col -> new TableCell<Player, Double>() {

            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (empty) {
                    setText(null);
                } else {
                    if (item == -1) {
                        setText("");
                    } else {
                        setText("");
                    }
                }
            }

        });      
    }
    private void linkColumntoData() {

        itemPosition.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("position"));
        itemFirst.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("first"));
        itemLast.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("last"));
        itemProTeam.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("team"));
        itemPositions.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("qp"));
        itemR_W.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("w_r"));
        itemHR_SV.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("hr_sv"));
        itemRBI_K.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("k_rbi"));
        itemSB_ERA.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("sb_era"));
        itemBA_WHIP.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("ba_whipString"));
        itemEstivatedValue.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("estimatedValue"));
        itemContract.setCellValueFactory(new PropertyValueFactory<TeamMember, String>("contract"));
        itemSalary.setCellValueFactory(new PropertyValueFactory<TeamMember, Integer>("salary"));

        lineupTable.getColumns().addAll(itemPosition, itemFirst, itemLast, itemProTeam, itemPositions,
                itemR_W, itemHR_SV, itemRBI_K, itemSB_ERA, itemBA_WHIP, itemEstivatedValue,
                itemContract, itemSalary);

        

        itemEstivatedValue.setCellFactory(col -> new TableCell<Player, Double>() {

            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (empty) {
                    setText(null);
                } else {
                    setText("");
                }
            }

        });      
    }
    //GETTER AND SETTER
    public TeamDataManager getTdataManager() {
        return tdataManager;
    }

    public TeamEditController getTeamEditController() {
        return teamEditController;
    }

    public ComboBox getTeamComboBox() {
        return teamComboBox;
    }

    public TableView<TeamMember> getLineupTable() {
        return lineupTable;
    }

    public TextField getDraftTextField() {
        return draftTextField;
    }

    public TableView<TeamMember> getTaxiTable() {
        return taxiTable;
    }

    

}
