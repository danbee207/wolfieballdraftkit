package wdf.gui;

import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdf.WDK_PropertyType;
import static wdf.WDK_StartupConstants.*;
import wdf.controller.DraftEditController;
import wdf.controller.FileController;
import wdf.controller.PlayerEditController;
import wdf.controller.ScreenModeController;
import wdf.controller.TeamEditController;
import wdf.data.Draft;
import wdf.data.DraftDataView;
import wdf.data.Player;
import wdf.file.DraftSiteExporter;
import wdf.file.WolfieballFileManager;
import wdf.manager.DraftDataManager;
import wdf.manager.PlayerDataManager;
import wdf.manager.TeamDataManager;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Draft and exporting it to a
 * site.
 *
 * @author DnB
 */
public class WDK_GUI implements DraftDataView {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_gui_style.css";
    static final String CLASS_BOREDERED_PANE = "bordered_pane";
    static final String CLASS_TOPBUTTON = "topbutton";
    static final String CLASS_TOPPANE = "toppane";
    static final String CLASS_WORKSCREEN = "workscreen";
    static final String CLASS_BOTTOMBUTTON = "bottombutton";

    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    
    
    //DATA MANAGER
    DraftDataManager draftDataManager;
    TeamDataManager teamManager;
    PlayerDataManager playermanager;

    //THIS MANAGER DRAFT FILE I/O
    WolfieballFileManager wolfieballFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    DraftSiteExporter siteExporter;

    // CONTROLLERs
    FileController fileController;
    ScreenModeController screenController;

    DraftEditController draftController;
    PlayerEditController playerController;
    TeamEditController teamController;

    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane wdkPane;

    HBox logoshot;

    //THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    HBox topToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button saveAsDraftButton;
    Button exportSiteButton;
    Button exitButton;

    //THIS IS THE BOTTOM TOOLBAR AND ITS CONTROLS
    HBox bottomToolbarPane;
    ToggleButton playerBtn;
    ToggleButton teamBtn;
    ToggleButton standingBtn;
    ToggleButton draftBtn;
    ToggleButton mlbBtn;

    //THIS IS THE WORKSPACE SPACE
    ScrollPane workspaceScrollPane;
    private TeamsScreen teamSpace;
    private PlayerScreen playerSpace;
    private DraftScreen draftSpace;
    private StandingScreen standingSpace;
    private MLBScreen mlbSpace;

    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
  //  WorkScreen workspacePane;
    boolean workspaceActivated;

    //HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    ProgressDialog progressDialog;

    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initGUI.
     *
     * @param primaryStage Window inside which the GUI will be displayed.
     */
    public WDK_GUI(Stage primaryStage) {
        this.primaryStage = primaryStage;

        teamManager = new TeamDataManager();

    }

    /**
     * Accessor method for the course file manager.
     *
     * @return The WolfieballFileManager used by this UI.
     */
    public WolfieballFileManager getWolfieballFileManager() {
        return wolfieballFileManager;
    }

    /**
     * Accessor method for the site exporter.
     *
     * @return The DraftSiteExporter used by this UI.
     */
    public DraftSiteExporter getSiteExporter() {
        return siteExporter;
    }

    public TeamDataManager getTeamManager() {
        return teamManager;
    }

    public TeamEditController getTeamController() {
        return teamController;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    /**
     * Mutator method for the draft data manager.
     *
     * @param dataManager The DraftDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager dataManager) {
        this.draftDataManager = dataManager;
    }

    /**
     * Mutator method for the course file manager.
     *
     * @param wolfieballFileManager The WolfieballFileManger to be used by this
     * UI.
     */
    public void setWolfieballFileManager(WolfieballFileManager wolfieballFileManager) {
        this.wolfieballFileManager = wolfieballFileManager;
    }

    /**
     * Mutator method for the site exporter.
     *
     * @param siteExporter The DraftSiteExporter to be used by this UI.
     */
    public void setSiteExporter(DraftSiteExporter siteExporter) {
        this.siteExporter = siteExporter;
    }

    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @param hitters THE list of hitters.
     * @param pitchers The list of pitchers.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initGUI(String windowTitle, ObservableList<Player> hitters, ObservableList<Player> pitchers) throws IOException {

        //INIT THE DIALOG
        initDialogs();
        
        //INIT THE CONTROLS
        initControls();

        //INIT THE TOOLBAR
        initTopToolbar();
        
        //INIT THE CENTER WORKSPACE CONTROLL BUT DON'T ADD THEM 
        //TO THE WINDOW YET
        initWorkspace();
        
        //INIT THE BOTTOM TOOLBAR
        initBottomToolbar();

        logoscene();

        //NOW SETUP THE EVENT HANDLERS
        initEventHandlers();



        //AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }

    /**
     * This function takes all of the data out of the draftToReload argument and
     * loads its values into the user interface controls.
     *
     * @param draftToReload The Draft whose data we'll load into the GUI.
     */
    @Override
    public void reloadDraft(Draft draftToReload) {

        if (!workspaceActivated) {
            activateWorkspace();
        }

        // WE DON'T WANT TO RESPOND TO EVENTS FORCED BY
        // OUR INITIALIZATION SELECTIONS
        draftController.enable(false);
        
        playerSpace.getSearchTxtField().textProperty().setValue("");
        playerSpace.getRadioGrup().selectToggle(playerSpace.all);
        playerSpace.getItemsTable().setItems(draftDataManager.getDraft().getFreeagent().getTeamMember());
        
        teamSpace.getDraftTextField().textProperty().setValue("");
        teamSpace.getTeamComboBox().getItems().clear();
        for(int i=1;i<draftDataManager.getDraft().getTeams().size();i++)
            teamSpace.getTeamComboBox().getItems().add(draftDataManager.getDraft().getTeams().get(i).getName());
//        teamSpace.getLineupTable().getItems().clear();
        //
        draftSpace.getDraftTable().setItems(draftDataManager.getDraft().getDrafted());
        
        draftController.enable(true);

    }

    /**
     * *************************************************************************
     */
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /**
     * *************************************************************************
     */
    private void initDialogs() {

        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);

    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initTopToolbar() {
        topToolbarPane = new HBox();


        //HERE ARE OUR TOP TOOLBAR BUTTONS, NOTE THAT SOME WILL 
        //START AS ENABLED (FALSE), WHILE OTHERS DISABLED(TURE)
        newDraftButton = initChildButton(topToolbarPane, WDK_PropertyType.NEW_DRAFT_ICON, WDK_PropertyType.NEW_DRAFT_TOOLTIP, false,CLASS_TOPBUTTON,new Insets(5));
        loadDraftButton = initChildButton(topToolbarPane, WDK_PropertyType.LOAD_DRAFT_ICON, WDK_PropertyType.LOAD_DRAFT_TOOLTIP, false,CLASS_TOPBUTTON,new Insets(5));
        saveDraftButton = initChildButton(topToolbarPane, WDK_PropertyType.SAVE_DRAFT_ICON, WDK_PropertyType.SAVE_DRAFT_TOOLTIP, true,CLASS_TOPBUTTON,new Insets(5));
        saveAsDraftButton = initChildButton(topToolbarPane, WDK_PropertyType.SAVEAS_DRAFT_ICON, WDK_PropertyType.SAVEAS_DRAFT_TOOLTIP, false,CLASS_TOPBUTTON,new Insets(5));
        exportSiteButton = initChildButton(topToolbarPane, WDK_PropertyType.EXPORT_DRAFT_ICON, WDK_PropertyType.EXPORT_DRAFT_TOOLTIP, false,CLASS_TOPBUTTON,new Insets(5));
        exitButton = initChildButton(topToolbarPane, WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false,CLASS_TOPBUTTON,new Insets(5));

        topToolbarPane.getStyleClass().add(CLASS_TOPPANE);
        topToolbarPane.setPadding(new Insets(5, 2, 5, 5));
        topToolbarPane.setSpacing(2.5);
    }

    /**
     * This function initializes all the buttons in the toolbar at the bottom of
     * the application window. These are related to screen management.
     */
    private void initBottomToolbar() {

        bottomToolbarPane = new HBox();
        bottomToolbarPane.setSpacing(3);
        bottomToolbarPane.setPadding(new Insets(0, 10, 5, 10));

        //HERE ARE OUR BOTTOM TOOLBAR BUTTONS, NOTE THAT SOME WILL 
        playerBtn = initChildToggleButton(bottomToolbarPane, WDK_PropertyType.PLAYER_ICON, WDK_PropertyType.PLAYER_TOOLTIP,false,CLASS_BOTTOMBUTTON,Insets.EMPTY);
        teamBtn = initChildToggleButton(bottomToolbarPane, WDK_PropertyType.TEAM_ICON, WDK_PropertyType.TEAM_TOOLTIP,false,CLASS_BOTTOMBUTTON,Insets.EMPTY);
        standingBtn = initChildToggleButton(bottomToolbarPane, WDK_PropertyType.TEAMSTANDING_ICON, WDK_PropertyType.STANDING_TOOLTIP,false,CLASS_BOTTOMBUTTON,Insets.EMPTY);
        draftBtn = initChildToggleButton(bottomToolbarPane, WDK_PropertyType.DRAFT_ICON, WDK_PropertyType.DRAFT_TOOLTIP,false,CLASS_BOTTOMBUTTON,Insets.EMPTY);
        mlbBtn = initChildToggleButton(bottomToolbarPane, WDK_PropertyType.MLB_ICON, WDK_PropertyType.MLB_TOOLTIP,false,CLASS_BOTTOMBUTTON,Insets.EMPTY);

        teamBtn.setSelected(true);
    }

    public void activateWorkspace() {

        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            wdkPane.setCenter(workspaceScrollPane);
            bottomToolbarPane.setVisible(true);
            workspaceActivated = true;
        }

    }

    private void logoscene() {
        logoshot = new HBox();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(WDK_PropertyType.LOGO.toString());
        Image logo = new Image(imagePath);
        logoshot.getChildren().add(new ImageView(logo));
        logoshot.setAlignment(Pos.CENTER);

    }
    
    private void initControls(){
        draftController = new DraftEditController(primaryStage,messageDialog,yesNoCancelDialog,this);
        playerController = new PlayerEditController(primaryStage, messageDialog, yesNoCancelDialog, this);
        teamController = new TeamEditController(primaryStage, messageDialog, yesNoCancelDialog, this);
    }

    private void initWorkspace() {

        workspaceScrollPane = new ScrollPane();
        workspaceScrollPane.getStyleClass().add(CLASS_WORKSCREEN);
  //      workspacePane.setPadding(Insets.EMPTY);

        teamSpace = new TeamsScreen(this);
        playerSpace = new PlayerScreen(this);
        draftSpace = new DraftScreen(this);
        standingSpace = new StandingScreen(this);
        mlbSpace = new MLBScreen(this);
        
        initScreens();
        workspaceScrollPane.setContent(teamSpace);
        workspaceScrollPane.setFitToWidth(true);
    

        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }
    
        private void initScreens() {
        teamSpace.initLayout();
        playerSpace.initLayout();
        draftSpace.initLayout();
        mlbSpace.initLayout();
      

    }
        private void initLocate() {

 
      
    }

    private void initEventHandlers() throws IOException {

        //FIRST THE FILE CONTROLS
        fileController = new FileController(messageDialog, yesNoCancelDialog, progressDialog, wolfieballFileManager, siteExporter);

        newDraftButton.setOnAction(e -> {
            fileController.handleNewDraftRequest(this);

        });
        loadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);

        });
        saveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this,this.getDataManager().getDraft(),this.getDataManager().getDraft().getSaveToName());

        });
        saveAsDraftButton.setOnAction(e -> {
            fileController.handleSaveAsDraftRequest(this,this.getDataManager().getDraft(),this.getDataManager().getDraft().getSaveToName());

        });
        exportSiteButton.setOnAction(e -> {
            fileController.handleExportDraftRequest(this);

        });
        exitButton.setOnAction(e -> {
            fileController.handleExitDraftRequest(this);

        });

        //SECOND SCRENN CONTROLL
        screenController = new ScreenModeController(workspaceScrollPane, this);
        
        playerBtn.setOnAction(e -> {
            closeotherBtn(playerBtn,true);
            screenController.handlePlayerScreenRequest();
            
        });

        teamBtn.setOnAction(e -> {
            closeotherBtn(teamBtn,true);    
            screenController.handleTeamScreenRequest();
        });
        standingBtn.setOnAction(e -> {
            closeotherBtn(standingBtn,true);
            standingSpace.initLayout();
            screenController.handleStandingScreenRequest();
            
        });
        draftBtn.setOnAction(e -> {
            closeotherBtn(draftBtn,true);
            screenController.handleDraftScreenRequest();
        });
        mlbBtn.setOnAction(e -> {
            closeotherBtn(mlbBtn,true);
            screenController.handleMLBScreenRequest();
        });

        //THEN THE DRAFT EDITING CONTRLS

    }
    private void closeotherBtn(ToggleButton btn,boolean ch){
    
        playerBtn.setSelected(!ch);
        teamBtn.setSelected(!ch);
        standingBtn.setSelected(!ch);
        draftBtn.setSelected(!ch);
        mlbBtn.setSelected(!ch);
        
        btn.setSelected(ch);
        
    }

    /**
     * INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS THERE EXCEPT
     * THE WORKSPACE, WHICH WILL BE ADDED THE FIRST TIME A NEW DRAFT IS CREATED
     * OR LOADED
     *
     * @param windowTitle
     */
    private void initWindow(String windowTitle) {

        //SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        //GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        
        //AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        wdkPane = new BorderPane();
        wdkPane.setTop(topToolbarPane);
        wdkPane.setCenter(logoshot);
        wdkPane.setBottom(bottomToolbarPane);
        bottomToolbarPane.setVisible(false);
        
        primaryScene = new Scene(wdkPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();

    }

    /**
     *
     * INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
     *
     * @param topToolbarPane
     * @param wdk_PropertyType
     * @param wdk_PropertyType0
     * @param b
     * @return
     */
    private Button initChildButton(HBox toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled, String classStyle,Insets padding) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);

        button.setGraphic(new ImageView(buttonImage));
        button.getStyleClass().add(classStyle);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        toolbar.getChildren().add(button);
        button.setTooltip(buttonTooltip);
        button.setPadding(padding);

        button.setMinHeight(25.0);

        return button;
    }
    private ToggleButton initChildToggleButton(HBox toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled, String classStyle,Insets padding) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        ToggleButton button = new ToggleButton();
        button.setDisable(disabled);

        button.setGraphic(new ImageView(buttonImage));
        button.getStyleClass().add(classStyle);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        toolbar.getChildren().add(button);
        button.setTooltip(buttonTooltip);
        button.setPadding(padding);

        button.setMinHeight(25.0);

        return button;
    }

    /**
     * This method is used to activate/deactivate toolbar buttons when they can
     * and cannot be used so as to provide foolproof design.
     *
     * @param saved
     */
    public void updateTopToolbarControls(boolean saved) {

        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveDraftButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadDraftButton.setDisable(false);
        exportSiteButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    public PlayerEditController getPlayerController() {
        return playerController;
    }

    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */

    public DraftDataManager getDataManager() {
        return draftDataManager;
    }

    /**
     * Accessor method for the file controller.
     *
     * @return The FileController used by this UI.
     */
    public FileController getFileController() {
        return fileController;
    }

    public PlayerDataManager getPlayermanager() {
        return playermanager;
    }

    public void setPlayermanager(PlayerDataManager playermanager) {
        this.playermanager = playermanager;
    }

    public TeamsScreen getTeamSpace() {
        return teamSpace;
    }

    public PlayerScreen getPlayerSpace() {
        return playerSpace;
    }

    public StandingScreen getStandingSpace() {
        return standingSpace;
    }

    public MLBScreen getMlbSpace() {
        return mlbSpace;
    }

    public DraftScreen getDraftSpace() {
        return draftSpace;
    }

    public void setTeamSpace(TeamsScreen teamSpace) {
        this.teamSpace = teamSpace;
    }

    public void setPlayerSpace(PlayerScreen playerSpace) {
        this.playerSpace = playerSpace;
    }

    public void setDraftSpace(DraftScreen draftSpace) {
        this.draftSpace = draftSpace;
    }

    public void setStandingSpace(StandingScreen standingSpace) {
        this.standingSpace = standingSpace;
    }

    public void setMlbSpace(MLBScreen mlbSpace) {
        this.mlbSpace = mlbSpace;
    }

    public DraftEditController getDraftController() {
        return draftController;
    }

  

    
    
}
