package wdf.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static wdf.WDK_StartupConstants.PATH_CSS;

/**
 * This class serves to present a dialog with three options to
 * the user: Yes, No, or Cancel and lets one access which was
 * selected.
 * 
 * @author DnB
 */
public class YesNoCancelDialog extends Stage{
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "Dialog_style.css";
    static final String CLASS_BUTTON = "button";
    static final String CLASS_LABEL = "heading_label";
    
    //GUI CONTROLS FOR OUR DIALOG
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Button yesButton;
    Button noButton;
    Button cancelButton;
    String selection;
    
    //CONSTANT CHOICES
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String CANCEL = "Cancel";
    
    
    /**
     * Initializes this dialog so that it can be used repeatedly
     * for all kinds of messages.
     *  
     * @param primaryStage The owner of this modal dialog.
     */
    
    public YesNoCancelDialog(Stage primaryStage){
        
        //MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        //FOR IT WHEN IT IS DISPLAYED
        
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();
       
        EventHandler yesNoCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae)->{
            Button sourceButton = (Button)ae.getSource();
            YesNoCancelDialog.this.selection = sourceButton.getText();
            YesNoCancelDialog.this.hide();
        };
        
        //YES,NO, AND CANCEL BUTTONS
        yesButton = new Button(YES);
        noButton = new Button(NO);
        cancelButton = new Button(CANCEL);
        yesButton.   setOnAction(yesNoCancelHandler);
        noButton .   setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(yesNoCancelHandler);
        
      
        
        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(yesButton);
        buttonBox.getChildren().add(noButton);
        buttonBox.getChildren().add(cancelButton);
        
        
        
        
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(buttonBox);
        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
        
    }

    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public void show(String message){
       // messageScene.getStylesheets().add(YesNoCancelDialog.class.getResource(PRIMARY_STYLE_SHEET).toExternalForm());
       
        messageLabel.setText(message);
        initStylesheet();
        this.showAndWait();
    }
    
    private void initStylesheet(){
        
        messageScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        messageLabel.getStyleClass().add(CLASS_LABEL);
        
        //Add STYLE TO BUTTON
        yesButton.getStyleClass().add(CLASS_BUTTON);
        noButton.getStyleClass().add(CLASS_BUTTON);
        cancelButton.getStyleClass().add(CLASS_BUTTON);
        
    }
}
