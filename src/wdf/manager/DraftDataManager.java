/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.manager;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wdf.data.*;
import wdf.file.WolfieballFileManager;
/**
 * This class manages a draft, which means it knows how to
 * reset one with default values and generate useful dates.
 *
 * @author DnB
 */
public class DraftDataManager {
    //THIS IS THE DRAFT BEING EDITED
    Draft draft;
    
    //THIS IS THE UI, WHICH MUST BE UPDATED
    //WHENEVER OUR MODEL'S DATA CHANGES
    DraftDataView view;
    
    //THIS IS HELPS US LOAD THINGS FOR OUR DRAFT
    WolfieballFileManager fileManger;
    
    //DEFAULT INITIALIZATION VALEUS FOR NEW DRAFT
    
    

    public DraftDataManager(DraftDataView initView,ObservableList<Player> hitters,ObservableList<Player> pitchers,ObservableList<Player> all){
        view = initView;
        draft = new Draft(hitters,pitchers,all);
       
    }
    /**
     *Accessor method for getting the Draft that this class manages.
     * @param initView 
     */
    public Draft getDraft() {
        return draft;
    }

    /**
     * Accessor method for getting the file manager, which knows how
     * to read and write draft data from/to files.
     */
    public WolfieballFileManager getFileManger() {
        return fileManger;
    }
    
    public void reset(){
        
        draft.setTeams(FXCollections.observableArrayList());
        draft.setDrafted(FXCollections.observableArrayList());
        draft.getTeams().add(draft.getFreeagent());
        // AND THEN FORCE THE UI TO RELOAD THE UPDATED DRAFT
        view.reloadDraft(draft);
        
        
        
    }
    
}
