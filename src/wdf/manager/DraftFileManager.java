/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdf.manager;

import java.io.IOException;
import wdf.data.Draft;

/**
 *
 * @author DnB
 */
public interface DraftFileManager {
    public void saveDraft(Draft draft) throws IOException;
    public void loadDraft(Draft draft, String draftPath) throws IOException;
    
}
